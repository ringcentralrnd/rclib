#pragma once
#include <string>
#include <algorithm> 

namespace sip_lib {

    inline bool iequals(const std::string &aStr1, const std::string &aStr2)
    {
        std::string str1(aStr1), str2(aStr2);
        std::transform(str1.begin(), str1.end(), str1.begin(), ::toupper);
        std::transform(str2.begin(), str2.end(), str2.begin(), ::toupper);
        return 0 == strcmp(str1.c_str(), str2.c_str());
    }
} // namespace sip_lib 

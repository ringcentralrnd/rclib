#pragma once

#include <queue>

template<typename Data>
class concurrent_queue
{
private:
  std::queue<Data> the_queue;
    
  mutable std::mutex the_mutex;
  typedef std::lock_guard<std::mutex> lock_t;

public:

  void push(Data const& data)
  {
    lock_t lock(the_mutex);
    the_queue.push(data);
  }

  bool empty() const
  {
    lock_t lock(the_mutex);
    return the_queue.empty();
  }

  bool try_pop(Data& popped_value)
  {
    lock_t lock(the_mutex);
    if(the_queue.empty())
    {
      return false;
    }

    popped_value=the_queue.front();
    the_queue.pop();
    return true;
  }

};

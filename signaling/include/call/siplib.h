#pragma once

#include "call/libs.h"
#include "call/concurrent_queue.h"
#include "call/register_session.h"
#include "call/invite_session.h"
#include "call/im.h"

namespace sip_lib_internal {

    static const char* realm = "*";
    static const char* digest = "digest";
    static pj_uint16_t udp_local_port = 0;
    static pj_uint16_t tcp_local_port = 0;

    class siplib : public shared_sip
    {

    public:

        typedef std::auto_ptr<std::thread> thread_ptr;
        typedef std::list<invite_session::ptr> calls_t;

        // sip_lib::transport is workaround 
        // pjsip dosent allow us to have more than one endpoint, 
        // so we cant have more than one account in parallel 
        // yes I know that is bad design
        // for good design the transport can be part of account
        siplib(sip_lib::notifier & a_notify, sip_lib::account_t const& a_account);
        virtual ~siplib(void);

        // create registration bind
        void bind_register(sip_lib::async_operation_result::ptr result);
        void unbind_register(sip_lib::async_operation_result::ptr result);

        //   call interface
        void make_call(sip_lib::call::ptr const& a_call, std::string const& a_to, sip_lib::async_operation_result::ptr result);
        // hang up can be call in any case UAC/UAS Trying/Answered etc
        void hang_up(sip_lib::call::ptr const& a_call, sip_lib::async_operation_result::ptr result);
        void answer(sip_lib::call::ptr const& a_call, sip_lib::async_operation_result::ptr result);
        void hold(sip_lib::call::ptr const& a_call, bool hold, sip_lib::async_operation_result::ptr result);
        void transfer(sip_lib::call::ptr const& a_call, std::string const& a_to, sip_lib::async_operation_result::ptr result);

        // utils
        void send_voice_message(
            std::string const& a_to,
            std::string const& text,
            sip_lib::async_operation_result::ptr sending_result,
            sip_lib::sip_response::ptr answer);

        // tools
        std::string const& network_interafce() const;

        void registration_failed(int status) const
        {
            LOGE("Registration stopped: %d", status);
            notify.on_register_fails(status);
        }

        void registration_successful(int exp_time_sec)  const
        {
            notify.on_registered(exp_time_sec);
        }

    private:

        void on_make_call(sip_lib::call::ptr const& a_call, std::string const& a_to);
        void on_hang_up(sip_lib::call::ptr const& a_call);
        void on_answer(sip_lib::call::ptr const& a_call);
        void on_hold(sip_lib::call::ptr const& a_call, bool hold);
        void on_transfer(sip_lib::call::ptr const& a_call, std::string const& a_to);
        void on_send_voice_message(std::string const& a_to, std::string const& text, sip_lib::sip_response::ptr answer);

        void async_call(job const& fun) const;
        std::string create_contact(sip_lib::account_t const& a_account) const;
        pjsip_endpoint* endpoint() const;
        pj_pool_t* pool() const;

        pjsip_module* register_handler() const 
        { 
            assert(register_event_handler.id!=-1);
            //  PJ lib workaround
            return &(const_cast<siplib*>(this)->register_event_handler);
        }

        virtual sip_lib::codec_description const& codec_selection_answer(sip_lib::codec_list const& offer) const
        {
            return notify.codec_selection_answer(offer);
        }

        virtual sip_lib::codec_list codec_selection_offer() const
        {
            return notify.codec_selection_offer();
        }

        void free(std::string call_id);
        void push(invite_session::ptr const& a_call);
        calls_t::iterator find(std::string const& call_id);

        static void log( int level, const char *buf, int len );
        pj_status_t pj_lib_init();
        pj_status_t invite_init();
        pj_status_t register_init();
        pj_status_t refer_init();
        pj_status_t im_init();
        void transport_init();

        void loop();
        static pj_bool_t on_rx_request(pjsip_rx_data *rdata);
        pj_bool_t rx_request(pjsip_rx_data *rdata);

        static void pjsip_tp_state_callback(pjsip_transport *tp, pjsip_transport_state state, const pjsip_transport_state_info *info);

        pjsip_endpoint*                 pj_endpt; 
        pj_caching_pool                 pj_cpool;
        pj_pool_t*					    pj_pool;

        thread_ptr                      pj_thread;
        bool                            to_stop;

        // one per ua (user agent)
        register_session::ptr           registrar;
        calls_t                         calls;     
        // one per ua (user agent)
        im_session::ptr                 im;

        pjsip_inv_callback              inv_session_event_handler;
        pjsip_module                    register_event_handler;
        pjsip_module                    im_event_handler;

        std::string                     contact;

        mutable concurrent_queue<job> jobs;

//         boost::asio::io_service io;
//         mutable boost::asio::deadline_timer timer;

        mutable std::mutex common_data_guard;

        std::string   eth;

        sip_lib::notifier& notify;

        pj_status_t schedule_timer(pj_timer_entry& timer, int time_out_sec) const;
        pj_status_t stop_timer(pj_timer_entry* timer, int id) const;
        pj_timer_heap_t *timer_heap;

        sip_lib::account_t account; // we need it for localy listenning, we cant use several account by PjSIP architecture restrictions

    };

    inline void fill_credentials(sip_lib::account_t& account, pjsip_cred_info& credentials)
    {
        pj_bzero(&credentials, sizeof(pjsip_cred_info));
        credentials.realm = pj_str_cast(realm);
        credentials.scheme = pj_str_cast(digest);
        credentials.username = pj_str_cast(account.auth_user);
        credentials.data_type = PJSIP_CRED_DATA_PLAIN_PASSWD;
        credentials.data = pj_str_cast(account.password);
    }


    inline void init_auth(pjsip_auth_clt_sess& auth_sess, shared_sip const& shared, sip_lib::account_t& account)
    {
        pj_status_t status;
        status = pjsip_auth_clt_init(&auth_sess, shared.endpoint(), shared.pool(), 0);
        THROW_IF_STATUS_FAILS(status, "pjsip_auth_clt_init failed");
        pjsip_cred_info credentials;
        fill_credentials(account,credentials);
        status = pjsip_auth_clt_set_credentials(&auth_sess, 1, &credentials);
        THROW_IF_STATUS_FAILS(status, "pjsip_auth_clt_set_credentials failed");
        pjsip_auth_clt_pref   auth_pref;
        auth_pref.algorithm = pj_str_cast("md5");
        auth_pref.initial_auth = 0;
        status = pjsip_auth_clt_set_prefs(&auth_sess, &auth_pref);
        THROW_IF_STATUS_FAILS(status, "pjsip_auth_clt_set_credentials failed");
    }

    inline pjsip_route_hdr* route_hdr(shared_sip const& shared, sip_lib::account_t& account)
    {
        std::stringstream ss2;
        if (account.transport == sip_lib::account_t::tcp)
            ss2 << "sip:" << account.proxy << ";transport=tcp" << ";lr";
        if (account.transport == sip_lib::account_t::udp)
            ss2 << "sip:" << account.proxy << ";transport=udp" << ";lr";
        std::string proxy_str = ss2.str();
        pjsip_route_hdr *route;
//        pjsip_route_hdr route_set;
        const pj_str_t hname = pj_str_cast("Route");
        //pj_list_init(&route_set);
        pj_str_t tmp;
        pj_strdup2_with_null(shared.pool(), &tmp, proxy_str.c_str());
        route = static_cast<pjsip_route_hdr *> (pjsip_parse_hdr(shared.pool(), &hname, tmp.ptr, tmp.slen, NULL));
//      pj_list_push_back(&route_set, route);
        return route;
    }

} // namespace sip_lib_internal 

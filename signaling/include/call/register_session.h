#pragma once

#include "call/libs.h"

namespace sip_lib_internal {

class register_session
{
public:
  typedef std::shared_ptr<register_session> ptr;
  friend class siplib;

  register_session(shared_sip& a_shared, sip_lib::account_t const& a_account);
  virtual ~register_session(void);
  void start();
  void stop();
  void refresh();

private:
  pj_status_t resend_register(pjsip_rx_data const* old_response = 0, pjsip_tx_data* old_request = 0, bool unbind = false);
  
  void stop_timer();

  static void on_tsx_state( pjsip_transaction *tsx, pjsip_event *event );
  void tsx_state( pjsip_transaction *tsx, pjsip_event *event );

  void on_error(int code);

  void restart_register(int seconds);

  shared_sip&           shared;
  sip_lib::account_t    account;
  std::string           contact;
  std::string           call_id;

  pjsip_auth_clt_sess   auth_sess;

  static void on_registration_timer(pj_timer_heap_t *timer_heap, struct pj_timer_entry *entry);
  void registration_timer(pj_timer_heap_t *timer_heap, struct pj_timer_entry *entry);

  pj_timer_entry* registrationTimer;

  int timer_id;
  int cseq;

private:
    register_session(const register_session&);

};


} // namespace sip_lib_internal 

#pragma once

#include "call/libs.h"

namespace sip_lib_internal {

    class refer_session
    {

    public:
        typedef std::auto_ptr<refer_session> ptr;
        friend class siplib;

        refer_session(shared_sip& a_shared, pjsip_dialog* a_dialog, pjsip_inv_session* a_session, sip_lib::call::ptr& a_call_face);
        virtual ~refer_session();
        void refer(std::string const& a_to);
        
    private:
        refer_session(const refer_session&);

    private:
        static void xfer_client_on_evsub_state(pjsip_evsub *sub, pjsip_event *event);
        void on_xfer_client_on_evsub_state(pjsip_evsub *sub, pjsip_event *event);

        shared_sip& shared;
        pjsip_dialog* dialog;
        pjsip_inv_session* session;

        sip_lib::call::ptr& call_face;

    };

} // namespace sip_lib_internal 

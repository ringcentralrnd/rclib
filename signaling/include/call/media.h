#pragma once

namespace sip_lib {

    struct codec_description
    {
        // http://en.wikipedia.org/wiki/RTP_audio_video_profile
        std::string mime_name;
        // sampling frequencies
        int rate;
        // payload type identifier
        std::string payload;
    };

    static const char* PCMU = ("PCMU");
    static const char* PCMU_PT = ("0");
    static int PCMU_RATE = 8000;

    static const char* G722 = ("g722");
    static const char* G722_PT = ("9");
    static int G722_RATE = 8000;

    static const char* G729 = ("g729");
    static const char* G729_PT = ("18");
    static int G729_RATE = 8000;

    static const char* DTMF = ("telephone-event");
    static const char* DTMF_PT = ("101");
    static int DTMF_RATE = 8000;

    static const char* OPUS = ("OPUS");
    // ������ ��� �����, ����� ������� ��� � ��� ����� RTCP,
    // � �� ����� ������������ ���������� OPUS �� ����� ������
    static const char* HM_OPUS_PT = ("105");

    static const char* ILBC = ("iLBC");
    static const char* ILBC_PT = ("102"); 
    static int ILBC_RATE = 8000;

    static const codec_description HOME_MADE_OPUS_CODEC = { OPUS, 16000, HM_OPUS_PT };
    static const codec_description PCMU_CODEC = { PCMU, PCMU_RATE, PCMU_PT };
    static const codec_description G722_CODEC = { G722, G722_RATE, G722_PT };
    static const codec_description ILBC_CODEC = { ILBC, ILBC_RATE, ILBC_PT };
    static const codec_description G729_CODEC = { G729, G729_RATE, G729_PT };


    // order makes sense 
    typedef std::list<codec_description> codec_list;

    struct media_connection_info
    {
        std::string ip;
        unsigned short port;
    };

    struct media_info
    {
        media_connection_info connection;
        codec_description codec;
    };

    struct media_offer_info
    {
        media_connection_info connection;
        codec_list codecs;
    };


} // namespace sip_lib 

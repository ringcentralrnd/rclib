#pragma once

#include <sstream>
#include <string>

namespace sip_lib {

    struct account_t
    {
        account_t()
            : transport(unknown)
        {}

        enum transport_type
        {
            unknown = 0, 
            udp,
            tcp,
            tls
        } transport;

        std::string user_alias;
        std::string user_name;
        std::string user_host;
        std::string uri() const
        {
            return account_t::uri(user_name, user_host, user_alias);
        }

        static std::string uri( std::string const& name_part, 
                                std::string const& host_part, 
                                std::string const& alias_part = std::string())
        {
            std::stringstream ss;
            if (alias_part.empty())
                ss << "<sip:" << name_part << "@" << host_part << ">";
            else
                ss << "\"" << alias_part << "\"" << " <sip:" << name_part << "@" << host_part << ">";
            return ss.str();
        }
        std::string proxy; // host:port
        std::string auth_user; // sip url
        std::string password; 

    };

} // namespace sip_lib 

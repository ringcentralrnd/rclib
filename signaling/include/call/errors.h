#pragma once

#include <exception>
#include <string>

namespace sip_lib {

    struct sip_exception : public std::exception
    {
        sip_exception(char const* a_description) throw()
        : description(a_description)
        {}
        const char * what() const throw ()
        {
            return description.c_str();
        }
        virtual ~sip_exception() throw() {};
        std::string description;
    };

    struct reason
    {
        enum source_t
        {
            EAPPLICATION = 0,
            ESIP
        } source;
        int code;
        std::string description;

        reason()
            : source(EAPPLICATION)
            , code(0)
        {}
    };

    struct async_operation_result
    {
        typedef std::shared_ptr<async_operation_result> ptr;
        virtual ~async_operation_result() {};
        virtual void error(std::exception const& ex) = 0;
        virtual void success() = 0;
    };

    struct sip_response
    {
        typedef std::shared_ptr<sip_response> ptr;
        virtual ~sip_response() {};
        virtual void on_result(unsigned short code) = 0;
    };
}


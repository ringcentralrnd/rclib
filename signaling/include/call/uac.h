#pragma once

#include <string>
#include <sstream>
#include <iostream>

#include "call/account.h"
#include "call/call.h"
#include "call/libs.h"


namespace sip_lib_internal
{
    class siplib;
    typedef std::shared_ptr<siplib> siplib_prt;
}

namespace sip_lib {

    class uac
    {
    public:

        typedef std::shared_ptr<uac> ptr;
        uac();

        // transport and notifier here is pjsip workaround, he dosent allow to have more than once endpoint
        // both should part of registration module, becase they are allow us to receive incoming calls
        void init(notifier& notify, account_t const& a_account);

        void bind_register(sip_lib::async_operation_result::ptr result = sip_lib::async_operation_result::ptr());
        void unbind_register(sip_lib::async_operation_result::ptr result = sip_lib::async_operation_result::ptr());

        std::string network_interafce() const;

        void make_call(sip_lib::call::ptr const& a_call, std::string const& to, sip_lib::async_operation_result::ptr result = sip_lib::async_operation_result::ptr());
        void hang_up(sip_lib::call::ptr const& a_call, sip_lib::async_operation_result::ptr result = sip_lib::async_operation_result::ptr());
        void answer(sip_lib::call::ptr const& a_call, sip_lib::async_operation_result::ptr result = sip_lib::async_operation_result::ptr());
        void hold(sip_lib::call::ptr const& a_call, bool hold, sip_lib::async_operation_result::ptr result = sip_lib::async_operation_result::ptr());
        void transfer(sip_lib::call::ptr const& a_call, std::string const& to, sip_lib::async_operation_result::ptr result = sip_lib::async_operation_result::ptr());

        void send_voice_message(std::string const& a_to, 
                                std::string const& text,
                                sip_lib::async_operation_result::ptr sending_result = sip_lib::async_operation_result::ptr(),
                                sip_lib::sip_response::ptr answer = sip_lib::sip_response::ptr());

        ~uac(void);
    private:
        sip_lib_internal::siplib_prt sip_impl;
    };


} // namespace sip_lib

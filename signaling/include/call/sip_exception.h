#pragma once

#include <exception>
#include <string>

namespace sip_lib {

struct sip_exception : public std::exception
{
  sip_exception(char const* a_description) throw()
     : description(a_description)
  {}
  const char * what () const throw ()
  {
    return description.c_str();
  }
  virtual ~sip_exception() throw () {};
  std::string description;
};

}


/*
 * call_manager.h
 *
 *  Created on: 30.01.2014
 *      Author: sergeygorun
 */

#ifndef CALL_MANAGER_H_
#define CALL_MANAGER_H_

//#include <boost/thread/thread.hpp>
#include <map>
#include <mutex>

#include "call/uac.h"
#include "call/account.h"
#include "call/call.h"




class call_manager {
private:

	typedef std::pair<int, sip_lib::call::ptr> pair;

	static std::mutex the_mutex;
	typedef std::lock_guard<std::mutex> lock_t;
	static call_manager *instance;

	sip_lib::uac::ptr _uac;
	sip_lib::account_t acc;

	std::map<long, sip_lib::call::ptr> native_calls;

	call_manager()
	{
		_uac =  sip_lib::uac::ptr( new sip_lib::uac() );
	}

public:
	static call_manager* get_instance();


	inline sip_lib::uac::ptr get_uac()
	{
		return _uac;
	}

	inline void dispose_uac()
	{
		LOGI("dispose account start");

		_uac.reset();
		_uac = sip_lib::uac::ptr( new sip_lib::uac() );

		LOGI("dispose account ok");
	}

	void init_uac(sip_lib::notifier& notify, const char* user_name, const char* auth_id,  const char* passwd, const char* host, const char* proxy);
	void bind_register();
	void make_call(sip_lib::call::ptr const& a_call, std::string const& to);

	inline void add_call(long n, sip_lib::call::ptr c)
	{
		native_calls.insert(pair(n, c));
	}

	inline void remove_call(long n)
	{
		if (native_calls.count(n) > 0)
		{
			native_calls.erase(n);
		}
	}

	inline sip_lib::call::ptr get_call(long n)
	{
		return native_calls[n];
	}
};

#endif /* CALL_MANAGER_H_ */

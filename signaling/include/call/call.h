#pragma once

#include <memory>

#include "call/account.h"
#include <list>
//#include <boost/algorithm/string.hpp>
#include "call/errors.h"
#include "call/media.h"

namespace sip_lib {

struct caller_info
{
	std::string user_name;
};

class call
{
public:
typedef std::shared_ptr<call> ptr;

// call every time, that media parameters has been chousen
virtual void on_media(media_info const& a_media) const = 0;

virtual media_info const& local_media() const = 0;

virtual void on_transfer_result(int err_sip_code) const = 0;

virtual void on_accepted(std::string const& call_id) const = 0;
virtual void on_terminated(std::string const& call_id, reason const& rs) const = 0;

virtual ~call() {}

std::string call_id;
};

class notifier
{
public:
	virtual call::ptr on_new_call(account_t const& a_account, caller_info const& caller) = 0;
	virtual void on_registered(int exp_time_sec) = 0;
	// codes:
	// 503 504 - server unavailable, check internet connectivity
	// 401 Unauthorized
	virtual void on_register_fails(int err_sip_code) = 0;
	virtual codec_description const& codec_selection_answer(codec_list const& offer) const = 0;
	virtual codec_list codec_selection_offer() const = 0;
	virtual ~notifier() {};
};


} // namespace sip_lib 

#pragma once

//  Precompile HEADER for any common

#include <memory>
#include <exception>
#include <string>
#include <sstream>
#include <iostream>
#include <algorithm> 
#include <thread>
#include <functional>
#include <mutex>

#ifdef _WIN32
#   ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#       define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#   endif						
#   define WIN32_LEAN_AND_MEAN
#   include <windows.h>
#endif

#pragma warning( push ) 
#pragma warning( disable : 4244 )
#include <pjsip.h>
#include <pjlib-util.h>
#include <pjlib.h>
#include <pjsip_ua.h>

#include <pj/config_site.h>
#include <pjsua-lib/pjsua.h>


#pragma warning( pop ) 


#if defined(TARGET_OS_IPHONE) || defined(TARGET_IPHONE_SIMULATOR)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wshorten-64-to-32"
#endif

#if defined(TARGET_OS_IPHONE) || defined(TARGET_IPHONE_SIMULATOR)
#pragma clang diagnostic pop
#endif

//#include <boost/enable_shared_from_this.hpp> 
// #include <boost/signals2/signal.hpp>
// #include <boost/signals2/trackable.hpp>

// #pragma warning( push ) 
// #pragma warning( disable : 4267 )
// #include <boost/asio.hpp>
// #pragma warning( pop ) 

#if defined(TARGET_OS_IPHONE) || defined(TARGET_IPHONE_SIMULATOR)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wuninitialized"
#pragma clang diagnostic ignored "-Wconditional-uninitialized"
#endif

//#include <boost/date_time/posix_time/posix_time.hpp>

#if defined(TARGET_OS_IPHONE) || defined(TARGET_IPHONE_SIMULATOR)
#pragma clang diagnostic pop
#endif

#include "call/utils.h"
#include "call/errors.h"
#include "call/account.h"
#include "call/call.h"

#include "call/logger.h"


namespace sip_lib_internal {

inline pj_str_t pj_str_cast(char*)
{
	throw sip_lib::sip_exception("wrong cast");
}

inline pj_str_t pj_str_cast(std::string const& str)
{
	pj_str_t pjstr = {const_cast<char*>(str.c_str()), static_cast<pj_ssize_t>(str.length())};
	return pjstr;
}

inline pj_str_t pj_str_cast(const char * str)
{
	return pj_str(const_cast<char*>(str));
}

#define EMPTY_PJSTR pj_str_cast("");


struct job
{
	job() {}
	typedef std::function<void()> job_fun_t;

	job(job_fun_t a_fun, sip_lib::async_operation_result::ptr a_error)
	    : fun(a_fun)
	    , error(a_error) {}

	job_fun_t fun;
	sip_lib::async_operation_result::ptr error;
};

class shared_sip
{
public:

	virtual pjsip_endpoint* endpoint() const = 0;
	virtual pj_pool_t* pool() const = 0;
	virtual std::string create_contact(sip_lib::account_t const& a_account) const = 0;
	virtual std::string const& network_interafce() const = 0;
	virtual pjsip_module* register_handler() const = 0;
	virtual void async_call(job const& fun) const = 0;
	virtual void registration_failed(int status) const = 0;
	virtual void registration_successful(int exp_time_sec) const = 0;
	virtual sip_lib::codec_description  const& codec_selection_answer(sip_lib::codec_list const& offer) const = 0;
	virtual sip_lib::codec_list codec_selection_offer() const = 0;
	virtual pj_status_t schedule_timer(pj_timer_entry& timer, int time_out_sec) const = 0;
	virtual pj_status_t stop_timer(pj_timer_entry* timer, int id) const = 0;
	// free call
	virtual void free(std::string call_id) = 0;
};

// TODO ERROR level log FILE STRING
#define DO_IF_STATUS_FAILS(status, description, do) {\
		if(status != PJ_SUCCESS) { \
			LOGE("%s, error code: %d", description, status); \
			do;  } }

#define THROW_IF_STATUS_FAILS(status, description) {\
		if(status != PJ_SUCCESS) { \
			LOGE("%s, error code: %d", description, status); \
			throw sip_lib::sip_exception(description);  } }

#define CALL_CHECK_THROW(a_call) {\
		if(!a_call) { \
			LOGE("bad argument: call"); \
			throw sip_lib::sip_exception("bad argument: call");  } }


} //namespace sip_lib_internal 

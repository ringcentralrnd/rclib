
#pragma once

#include "call/libs.h"

namespace sip_lib_internal {

    void initial_offer(shared_sip const& shared, sip_lib::call::ptr const& call_face, pjmedia_sdp_session** psdp, sip_lib::codec_list const& codecs);
    void hold_offer(shared_sip const& shared, sip_lib::call::ptr const& call_face, pjmedia_sdp_session** psdp, bool hold);
    // return success
    bool make_answer(shared_sip const& shared, sip_lib::call::ptr const& call_face, pjmedia_sdp_session** psdp, sip_lib::codec_list const& codecs);
    sip_lib::media_info extract_answer(pjsip_inv_session *inv);
    sip_lib::media_offer_info extract_offer(pjsip_inv_session *inv);

} // namespace sip_lib_internal 

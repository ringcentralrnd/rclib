
#include "call/libs.h"

namespace sip_lib_internal {

    class im_session
    {
    public:
        typedef std::shared_ptr<im_session> ptr;
        friend class siplib;

        im_session(shared_sip const& a_shared, sip_lib::account_t const& a_account);
        virtual ~im_session();
        void send_voice_message(std::string const& a_to, std::string const& text, sip_lib::sip_response::ptr answer);

    private:

        void send(const pj_str_t *to, const pj_str_t *mime_type, const char *msg_data, sip_lib::sip_response::ptr answer);

        static void im_callback(void *user_data, pjsip_event *e);
        void on_im_callback(pjsip_event *e, sip_lib::sip_response::ptr answer);
        
        pjsip_auth_clt_sess   auth_sess;

        shared_sip const&     shared;
        sip_lib::account_t             account;
        
    private:
        im_session(const im_session&);
    };


} // namespace sip_lib_internal 

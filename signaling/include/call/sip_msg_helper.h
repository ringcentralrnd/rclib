#ifndef SIP_MSP_HELPER_H_
#define SIP_MSP_HELPER_H_

inline pjsip_rx_data *get_rx_data(pjsip_event *e)
{
   if (e->type == PJSIP_EVENT_RX_MSG)
      return e->body.rx_msg.rdata;

   if (e->type == PJSIP_EVENT_TSX_STATE && e->body.tsx_state.type == PJSIP_EVENT_RX_MSG)
      return e->body.tsx_state.src.rdata;

   // There's no rdata on this event
   return NULL;
}


#endif /* SIP_MSP_HELPER_H_ */

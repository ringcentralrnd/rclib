#pragma once

#include "libs.h"
#include "refer_session.h"

namespace sip_lib_internal {

    class invite_session
    {
    public:
        typedef std::shared_ptr<invite_session> ptr;
        friend class siplib;
        friend class call_id_finder;

        invite_session(shared_sip& a_shared, std::string const& a_to, sip_lib::call::ptr const& a_call, sip_lib::account_t const& a_account);
        invite_session(shared_sip& a_shared, pjsip_rx_data *rdata, sip_lib::account_t const& a_account);
        virtual ~invite_session();

        void invite();
        void answer();
        void hangup();
        void hold(bool hold);
        void transfer(std::string const& a_to);

    private:
        void compose_and_send_invite();
        pj_status_t hangup_session();
        pj_status_t send_response();
        bool incomong_call_media_conversation();
        static void on_new_session(pjsip_inv_session *inv, pjsip_event *e);
        static void on_state_changed( pjsip_inv_session *inv, pjsip_event *e);
        void state_changed( pjsip_inv_session *inv, pjsip_event *e);
        static void on_media_update( pjsip_inv_session *inv, pj_status_t status);
        void media_update( pjsip_inv_session *inv, pj_status_t status);


        sip_lib::account_t account;
        shared_sip&         shared;
        pjsip_inv_session*    session;
        pjsip_dialog*         dialog;

        std::string contact;
        sip_lib::call::ptr call_face;

        pjsip_route_hdr route_set;

        refer_session::ptr refer;

        sip_lib::media_info media;
        
    private:
        invite_session(const invite_session&);

    };


}    // namespace sip_lib_internal 

/*
 * android_logger.h
 *
 *  Created on: 29 mai 2012
 *      Author: r3gis3r
 */

#ifndef ANDROID_LOGGER_H_
#define ANDROID_LOGGER_H_

#if defined(__APPLE__) || defined(__MACH__)
#include "TargetConditionals.h"
#endif

static const char* TAG = "libpjsip";


/** ANDROID */
#if defined(__ANDROID__)
#include <android/log.h>
#define LOG_ERROR ANDROID_LOG_ERROR
#define PJ_LOGGING(LOG_LEVEL, LOG_TAG, fmt, ...) \
{ \
    __android_log_vprint(LOG_LEVEL, LOG_TAG, fmt, ##__VA_ARGS__); \
}

/** WIN32 */
#elif defined(_WIN32) || defined(linux)
#define LOG_ERROR LINUX_LOG_ERROR
#define PJ_LOGGING(LOG_LEVEL, LOG_TAG, fmt, ...) \
{ \
    vprintf( fmt, ##__VA_ARGS__ ); \
    printf("\n"); \
}
/** IPHONE */
#elif defined(TARGET_OS_IPHONE) || defined(TARGET_IPHONE_SIMULATOR)
void RCCLogv(const char * __restrict format, va_list args);
#define PJ_LOGGING(LOG_LEVEL, LOG_TAG, fmt, ...) \
{ \
    RCCLogv(fmt, ##__VA_ARGS__ );\
}
#endif // _WIN32 __ANDROID__ TARGET_OS_IPHONE


#define LOGV(...) Log::log(2, __VA_ARGS__)
#define LOGD(...) Log::log(3, __VA_ARGS__)
#define LOGI(...) Log::log(4, __VA_ARGS__)
#define LOGW(...) Log::log(5, __VA_ARGS__)
#define LOGE(...) Log::log(6, __VA_ARGS__)

//const int level = 1;

class Log {
	static int level;

public:
	const static int LEVER_VERBOSE = 1;
	const static int LEVER_DEBUG = 2;
	const static int LEVER_INFO = 3;
	const static int LEVER_WARNING = 4;
	const static int LEVER_ERROR = 5;
	const static int LEVEL_DISABLED = -1;

	inline static void setLevel(int level_)
	{
		if (level_ > LEVER_ERROR)
			level = LEVER_ERROR;
		else if (level_ > 0)
			level = level_;
		else
			level_ = LEVEL_DISABLED;
	}

	inline static void log(int l, const char *fmt, ...)
	{
		if (l == LEVEL_DISABLED || l < level)
			return;

        va_list arglist;
        va_start( arglist, fmt );

        PJ_LOGGING(l, TAG, fmt, arglist);

        va_end( arglist );
	}

};

#endif /* ANDROID_LOGGER_H_ */


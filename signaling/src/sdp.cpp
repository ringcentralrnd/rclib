#include "call/utils.h"
#include "call/sdp.h"
#include <pjmedia/sdp_neg.h>
#include <vector>

namespace sip_lib_internal {

    typedef std::vector<pjmedia_sdp_rtpmap> rtp_map_t;


    char const* SDP_USER = "-";
    char const* SDP_IN = "IN";
    char const* SDP_PROTO = "IP4";
    char const* SDP_AGENT = "RC-SIP";
    char const* SDP_AUDIO = "audio";
    char const* SDP_TR = "RTP/AVP";

    char const* SDP_DIR_SR = "sendrecv";
    char const* SDP_DIR_S = "sendonly";
    char const* SDP_DIR_R = "recvonly";

    void offer(shared_sip const& shared, sip_lib::call::ptr const& call_face, pjmedia_sdp_session** psdp, rtp_map_t const& map, char const* direction);
    
    pjmedia_sdp_rtpmap pcmu()
    {
        pjmedia_sdp_rtpmap  rtpmap;
        rtpmap.param.ptr = 0;
        rtpmap.param.slen = 0;
        rtpmap.enc_name = pj_str_cast(sip_lib::PCMU);
        rtpmap.pt = pj_str_cast(sip_lib::PCMU_PT);
        rtpmap.clock_rate = sip_lib::PCMU_RATE;
        return rtpmap;
    }

    pjmedia_sdp_rtpmap g722()
    {
        pjmedia_sdp_rtpmap  rtpmap;
        rtpmap.param.ptr = 0;
        rtpmap.param.slen = 0;
        rtpmap.enc_name = pj_str_cast(sip_lib::G722);
        rtpmap.pt = pj_str_cast(sip_lib::G722_PT);
        rtpmap.clock_rate = sip_lib::G722_RATE;
        return rtpmap;
    }

    pjmedia_sdp_rtpmap g729()
    {
        pjmedia_sdp_rtpmap  rtpmap;
        rtpmap.param.ptr = 0;
        rtpmap.param.slen = 0;
        rtpmap.enc_name = pj_str_cast(sip_lib::G729);
        rtpmap.pt = pj_str_cast(sip_lib::G729_PT);
        rtpmap.clock_rate = sip_lib::G729_RATE;
        return rtpmap;
    }

    pjmedia_sdp_rtpmap ilbc()
    {
        pjmedia_sdp_rtpmap  rtpmap;
        rtpmap.param.ptr = 0;
        rtpmap.param.slen = 0;
        rtpmap.enc_name = pj_str_cast(sip_lib::ILBC);
        rtpmap.pt = pj_str_cast(sip_lib::ILBC_PT);
        rtpmap.clock_rate = sip_lib::ILBC_RATE;
        return rtpmap;
    }

    pjmedia_sdp_rtpmap home_made_opus()
    {
        pjmedia_sdp_rtpmap  rtpmap;
        rtpmap.param = pj_str_cast("1"); // mono 
        rtpmap.enc_name = pj_str_cast(sip_lib::OPUS);
        rtpmap.pt = pj_str_cast(sip_lib::HOME_MADE_OPUS_CODEC.payload);
        rtpmap.clock_rate = sip_lib::HOME_MADE_OPUS_CODEC.rate;
        return rtpmap;
    }

    pjmedia_sdp_rtpmap dtmf()
    {
        pjmedia_sdp_rtpmap  dtmf;
        dtmf.param.ptr = 0;
        dtmf.param.slen = 0;
        dtmf.enc_name = pj_str_cast(sip_lib::DTMF);
        dtmf.pt = pj_str_cast(sip_lib::DTMF_PT);
        dtmf.clock_rate = sip_lib::DTMF_RATE;
        return dtmf;
    }

    
    void codecs2pjrtpmap(sip_lib::codec_list const& codecs, rtp_map_t& map)
    {
        for (sip_lib::codec_list::const_iterator it = codecs.begin(), lim = codecs.end(); it != lim; ++it)
        {
            if (it->payload == sip_lib::PCMU_PT)
                map.push_back(pcmu());
            if (it->payload == sip_lib::G722_PT)
                map.push_back(g722());
            if (it->payload == sip_lib::G729_PT)
                map.push_back(g729());
            if (sip_lib::iequals(it->mime_name, sip_lib::ILBC))
                map.push_back(ilbc());
            if (sip_lib::iequals(it->mime_name, sip_lib::OPUS))
                map.push_back(home_made_opus());
        }
    }

    void initial_offer(shared_sip const& shared, sip_lib::call::ptr const& call_face, pjmedia_sdp_session** psdp, sip_lib::codec_list const& codecs)
    {
        rtp_map_t map;
        codecs2pjrtpmap(codecs, map);
        map.push_back(dtmf());
        offer(shared, call_face, psdp, map, SDP_DIR_SR);
    }


    void hold_offer(shared_sip const& shared, sip_lib::call::ptr const& call_face, pjmedia_sdp_session** psdp, bool hold)
    {
        rtp_map_t map;
        map.push_back(pcmu());
        map.push_back(dtmf());

        //    Older UAs may set the
        //    connection address to 0.0.0.0 when initiating hold.However, this
        //    behavior has been deprecated in favor or using the a = inactive SDP
        //    attribute if no media is sent, or the a = sendonly attribute if media
        //    is still sent.

        offer(shared, call_face, psdp, map, hold ? SDP_DIR_S : SDP_DIR_SR);
    }

    void offer(shared_sip const& shared, sip_lib::call::ptr const& call_face, pjmedia_sdp_session** psdp, rtp_map_t const& map, char const* direction)
	{
            try
            {
                //Create and initialize basic SDP session
                *psdp = static_cast< pjmedia_sdp_session* > (pj_pool_zalloc(shared.pool(), sizeof(pjmedia_sdp_session)));
                pjmedia_sdp_session* sdp = *psdp;

                sdp->origin.user = pj_str_cast(SDP_USER);
                sdp->origin.version = 802025568;
                sdp->origin.id = 1;

                sdp->origin.net_type = pj_str_cast(SDP_IN);
                sdp->origin.addr_type = pj_str_cast(SDP_PROTO);
                pj_strdup2_with_null(shared.pool(), &sdp->origin.addr, call_face->local_media().connection.ip.c_str());

                sdp->name = pj_str_cast(SDP_AGENT);

                //SDP time 
                sdp->time.start = 0;
                sdp->time.stop = 0;
              
                sdp->media_count = 1;

                // Create media stream
                pjmedia_sdp_media* sdpMedia = static_cast< pjmedia_sdp_media * >(pj_pool_zalloc(shared.pool(), sizeof(pjmedia_sdp_media)));
                sdp->media[0] = sdpMedia;
                // Standard media info
                sdpMedia->desc.media = pj_str_cast(SDP_AUDIO);
                sdpMedia->desc.port = call_face->local_media().connection.port;
                sdpMedia->desc.transport = pj_str_cast(SDP_TR);
                sdpMedia->desc.port_count = 1;

                sdpMedia->conn = static_cast< pjmedia_sdp_conn * >(pj_pool_zalloc(shared.pool(), sizeof(pjmedia_sdp_conn)));
                sdpMedia->conn->net_type = pj_str_cast(SDP_IN);
                sdpMedia->conn->addr_type = pj_str_cast(SDP_PROTO);
                pj_strdup2_with_null(shared.pool(), &sdpMedia->conn->addr, call_face->local_media().connection.ip.c_str());

                sdpMedia->desc.fmt_count = 0;
                sdpMedia->attr_count = 0;
                for (size_t i = 0, lim = map.size(); i < lim; ++i)
                {
                    pjmedia_sdp_rtpmap  rtpmap = map[i];
                    pjmedia_sdp_attr* attr = 0;
                    pjmedia_sdp_rtpmap_to_attr(shared.pool(), &rtpmap, &attr);
                    sdpMedia->attr[sdpMedia->attr_count++] = attr;

                    sdpMedia->desc.fmt[sdpMedia->desc.fmt_count++] = rtpmap.pt;
                }


                pjmedia_sdp_attr* dtmf_attr = PJ_POOL_ZALLOC_T(shared.pool(), pjmedia_sdp_attr);
                dtmf_attr->name = pj_str_cast("fmtp");
                dtmf_attr->value = pj_str_cast("101 0-16");
                sdpMedia->attr[sdpMedia->attr_count++] = dtmf_attr;

                pjmedia_sdp_attr* send_attr = static_cast< pjmedia_sdp_attr * >(pj_pool_zalloc(shared.pool(), sizeof(pjmedia_sdp_attr)));
                send_attr->name = pj_str_cast(direction);
                sdpMedia->attr[sdpMedia->attr_count++] = send_attr;

            }
            catch (const std::exception&)
            {
                return;
            }
            return;
}

// return success
bool make_answer(shared_sip const& shared, sip_lib::call::ptr const& call_face, pjmedia_sdp_session** psdp, sip_lib::codec_list const& codecs)
{
    rtp_map_t map;
    codecs2pjrtpmap(codecs, map);
    map.push_back(dtmf());
    offer(shared, call_face, psdp, map, SDP_DIR_SR);
	return true;
}

sip_lib::codec_list extract_codecs(pjmedia_sdp_session const *sdp)
{
    sip_lib::codec_list codecs;
    // 0 hardcoded, TODO dynamic find audio
    for (unsigned int i = 0; i< sdp->media[0]->desc.fmt_count; ++i)
    {
        pj_str_t pj_t = sdp->media[0]->desc.fmt[i];
        std::string pt(pj_t.ptr, pj_t.slen);
        pjmedia_sdp_rtpmap rtpmap_at;
        pj_bzero(&rtpmap_at, sizeof(rtpmap_at));


        for (unsigned int j = 0; j < sdp->media[0]->attr_count; ++j)
        {
            pjmedia_sdp_attr* at = sdp->media[0]->attr[j];
            if (sip_lib::iequals(std::string(at->name.ptr, at->name.slen), "rtpmap"))
            {
                pjmedia_sdp_rtpmap rtpmap_at_var;
                pjmedia_sdp_attr_get_rtpmap(at, &rtpmap_at_var);
                if (sip_lib::iequals(std::string(rtpmap_at_var.pt.ptr, rtpmap_at_var.pt.slen), pt))
                {
                    rtpmap_at = rtpmap_at_var;
                    break;
                }
            }
        }

        if (rtpmap_at.pt.slen > 0)
        {
            sip_lib::codec_description generic_codec;
            generic_codec.mime_name = std::string(rtpmap_at.enc_name.ptr, rtpmap_at.enc_name.slen);
            generic_codec.payload = std::string(rtpmap_at.pt.ptr, rtpmap_at.pt.slen);
            generic_codec.rate = rtpmap_at.clock_rate;
            // todo ignore extended params, channels for OPUS
            codecs.push_back(generic_codec);
        }
    }

    return codecs;
}


sip_lib::media_offer_info extract_offer(pjsip_inv_session *inv)
{
//    const pjmedia_sdp_session *remote_sdp = inv->neg->neg_remote_sdp;

    const pjmedia_sdp_session* remote_sdp = 0;
    pj_status_t status;
    sip_lib::media_offer_info media;


//    *remote_sdp = inv->neg->neg_remote_sdp;

    status = pjmedia_sdp_neg_get_neg_remote(inv->neg, &remote_sdp);
    DO_IF_STATUS_FAILS(status, "Remote SDP not found", return media);

    media.connection.ip = std::string(remote_sdp->origin.addr.ptr, remote_sdp->origin.addr.slen);
    media.connection.port = remote_sdp->media[0]->desc.port;
    media.codecs = extract_codecs(remote_sdp);

    return media;
}

sip_lib::media_info extract_answer(pjsip_inv_session *inv)
{
	const pjmedia_sdp_session *local_sdp;
	const pjmedia_sdp_session *remote_sdp;

	pj_status_t status;
	sip_lib::media_info media;

	status = pjmedia_sdp_neg_get_active_local(inv->neg, &local_sdp);
	DO_IF_STATUS_FAILS(status, "Local SDP not found", return media);

	status = pjmedia_sdp_neg_get_active_remote(inv->neg, &remote_sdp);
	DO_IF_STATUS_FAILS(status, "Remote SDP not found", return media);

    media.connection.ip = std::string(remote_sdp->origin.addr.ptr, remote_sdp->origin.addr.slen);
    media.connection.port = remote_sdp->media[0]->desc.port;

    sip_lib::codec_list codecs = extract_codecs(remote_sdp);
    sip_lib::codec_description final_codec;
    if (codecs.size() > 0)
        final_codec = *(codecs.begin());

    media.codec = final_codec;

	return media;
}

} // namespace sip_lib_internal {

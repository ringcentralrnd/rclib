#include "call/register_session.h"
#include "call/siplib.h"
#include "call/sip_msg_helper.h"

namespace sip_lib_internal {


register_session::register_session(shared_sip& a_shared, sip_lib::account_t const& a_account)
: shared(a_shared)
, contact(a_shared.create_contact(a_account))
, registrationTimer(0)
, timer_id(1)
, cseq(100)
{
	account = a_account; // copy to avoid changes
    init_auth(auth_sess, shared, account);
        
}

void register_session::on_registration_timer(pj_timer_heap_t *timer_heap, struct pj_timer_entry *entry)
{
    LOGD("on_registration_timer");
    if(entry->user_data)
        static_cast<register_session*>(entry->user_data)->registration_timer(timer_heap, entry);
}

void register_session::registration_timer(pj_timer_heap_t *, struct pj_timer_entry *)
{
    pj_status_t status = resend_register();
    DO_IF_STATUS_FAILS(status, "resend_register failed", on_error(status));
}

void register_session::start()
{
    pj_status_t status = resend_register();
    DO_IF_STATUS_FAILS(status, "resend_register failed", on_error(status));
}

void register_session::stop()
{
    pj_status_t status = resend_register(0,0,true);
    DO_IF_STATUS_FAILS(status, "resend_register failed", on_error(status));
    stop_timer();
}

void register_session::stop_timer()
{
    if (registrationTimer)
        shared.stop_timer(registrationTimer, timer_id);
    registrationTimer = 0; 
}

register_session::~register_session(void)
{
    stop_timer();
}

void register_session::restart_register(int seconds)
{
    // init timer
    LOGD("restart_register");
    stop_timer();
    registrationTimer =  new pj_timer_entry();
    pj_bzero(registrationTimer, sizeof(pj_timer_entry));
    registrationTimer->id = PJSUA_INVALID_ID;
    registrationTimer->cb = &on_registration_timer;
    timer_id++;
    pj_timer_entry_init(registrationTimer, timer_id, this, &register_session::on_registration_timer);
    pj_status_t status = shared.schedule_timer(*registrationTimer, seconds);
    DO_IF_STATUS_FAILS(status, "REGISTER timer stopped ", return);
}

void register_session::on_tsx_state( pjsip_transaction *tsx, pjsip_event *event )
{
	try
	{
		
		if( !tsx || !event )
		{
			LOGE( "Either tsx or event pointer is NULL" );
			return;
		}

        if (tsx->method.id != PJSIP_REGISTER_METHOD)
            return;

		register_session* _this = static_cast<register_session*>(tsx->mod_data[0]);
		if(_this)
			_this->tsx_state(tsx, event);
	}
	catch (std::exception & e)
	{
		LOGE( e.what() );
	}
}

bool has_auth_info(pjsip_tx_data* request)
{
    pjsip_authorization_hdr* hdr = (pjsip_authorization_hdr*)pjsip_msg_find_hdr(request->msg, PJSIP_H_AUTHORIZATION, NULL);
    if(hdr)
        return true;
    else
        return false;
}

void register_session::tsx_state( pjsip_transaction *tsx, pjsip_event *event )
{
	if ( !( event->type == PJSIP_EVENT_TSX_STATE && tsx->state > PJSIP_TSX_STATE_CALLING ) )
		return;

//    if (tsx->state == PJSIP_TSX_STATE_COMPLETED || tsx->state == PJSIP_TSX_STATE_TERMINATED)
//        stop_timer();

	if ( tsx->state == PJSIP_TSX_STATE_COMPLETED )
	{
		LOGI("PJSIP_TSX_STATE_COMPLETED");

        pjsip_rx_data *rdata = get_rx_data(event);
        int code = tsx->status_code;

        if ((code == PJSIP_SC_PROXY_AUTHENTICATION_REQUIRED ||   
            code == PJSIP_SC_UNAUTHORIZED)  &&
            !has_auth_info(tsx->last_tx))
        {
            LOGD("Unathorized, Resend REGISTER");
            pj_status_t status = resend_register(rdata, tsx->last_tx);
            DO_IF_STATUS_FAILS(status  , "resend_register failed", shared.registration_failed(PJSIP_SC_SERVICE_UNAVAILABLE));
            return;
        }

        if(code == PJSIP_SC_OK)
		{
			pjsip_msg *msg = rdata->msg_info.msg;
			pjsip_contact_hdr *exp_h =  (pjsip_contact_hdr*)pjsip_msg_find_hdr(msg, PJSIP_H_CONTACT, NULL);
			if (exp_h != NULL)
			{

                if (rdata->tp_info.transport) 
                    pjsip_transport_add_ref(rdata->tp_info.transport);

				int seconds = exp_h->expires;
				LOGI("expires: %d", seconds);
                shared.registration_successful(seconds);
                if (seconds)
                    restart_register(seconds);
			}
            else
            {
                LOGD("Un REGISTER response");
                shared.registration_successful(0);
            }

            return;
		}
		//		LOGD( "Received SIP %d %.*s for %s (%d)", tsx->status_code, tsx->status_text.slen, tsx->status_text.ptr, record->contact, record->deviceType );

        on_error(tsx->status_code);
	}
    else if (tsx->state == PJSIP_TSX_STATE_TERMINATED && 
                    tsx->status_code >= PJSIP_SC_MULTIPLE_CHOICES && 
                    tsx->status_code != PJSIP_SC_UNAUTHORIZED && 
                    tsx->status_code != PJSIP_SC_PROXY_AUTHENTICATION_REQUIRED)
         on_error(tsx->status_code);
}

void register_session::on_error(int code)
{
    shared.registration_failed(code);
}

pj_status_t register_session::resend_register(pjsip_rx_data const* old_response /* = 0 */, pjsip_tx_data* old_request /* = 0 */, bool unbind)
{
	LOGI("calling send register");

	pj_status_t status;
	pjsip_transaction *tsxTransaction;

    LOGI("compose_register_start ok");

    std::string to = account.uri();
    pj_str_t to_addr = pj_str_cast(to);

    pj_str_t contact_uri;
    {
        if (!unbind)
            contact_uri = pj_str_cast(contact);
        else
            contact_uri = pj_str_cast("*");
    }

    if (old_request)
    {
        pjsip_msg *msg = old_request->msg;
        pjsip_contact_hdr *exp_h = (pjsip_contact_hdr*)pjsip_msg_find_hdr(msg, PJSIP_H_CONTACT, NULL);
        if (exp_h != NULL)
        {
            if (exp_h->star)
                contact_uri = pj_str_cast("*");
        }
    }

    pj_str_t callid;
    pj_str_t* callidptr = NULL;  // put NULL to generate unique Call-ID
    if (!call_id.empty())
    {
        callid = pj_str_cast(call_id);
        callidptr = &callid;
    }

    std::stringstream ss;
    ss << "sip:" << account.user_host;
    std::string req_uri_str = ss.str();
    pj_str_t req_uri = pj_str_cast(req_uri_str);
    pjsip_tx_data* tdata = NULL;

    status = pjsip_endpt_create_request(shared.endpoint(),
        &pjsip_register_method,
        &req_uri,       // target URI
        &to_addr,       // From:
        &to_addr,       // To:
        &contact_uri,   // Contact:
        callidptr,        // Call-Id
        ++cseq,              // CSeq#
        NULL,           // body
        &tdata);

//    CSeq: The CSeq value guarantees proper ordering of REGISTER
//    requests.A UA MUST increment the CSeq value by one for each
//    REGISTER request with the same Call - ID.

    if (PJ_SUCCESS != status)
        DO_IF_STATUS_FAILS(status, "pjsip_endpt_create_request failed", return NULL);

    callid = PJSIP_MSG_CID_HDR(tdata->msg)->id;
    call_id = std::string(callid.ptr, callid.slen);

    pjsip_route_hdr *route = route_hdr(shared, account);
    pjsip_msg_add_hdr(tdata->msg, (pjsip_hdr*)route);

    if (tdata == NULL)
		DO_IF_STATUS_FAILS(PJSIP_ENOTINITIALIZED  , "compose_register failed", return PJSIP_ENOTINITIALIZED);

    if(old_response && old_request)
    {
        status = pjsip_auth_clt_reinit_req( &auth_sess,   
            old_response,    
            old_request,     
            &tdata);
        DO_IF_STATUS_FAILS(status  , "set auth failed failed", return status);

        pjsip_cseq_hdr* hdr2 = (pjsip_cseq_hdr*)pjsip_msg_find_hdr(tdata->msg, PJSIP_H_CSEQ, NULL);
        hdr2->cseq = cseq;
    }


    if (unbind)
    {
        pjsip_hdr* h = (pjsip_hdr*)pjsip_expires_hdr_create(shared.pool(), 0);
        pjsip_msg_add_hdr(tdata->msg, h);
    }

    status = pjsip_tsx_create_uac(shared.register_handler(), tdata, &tsxTransaction);
 	DO_IF_STATUS_FAILS(status, "pjsip_tsx_create_uac failed", return status);

	tsxTransaction->mod_data[0] = static_cast<void*>(this);

    status = pjsip_tsx_send_msg(tsxTransaction, tdata);
	DO_IF_STATUS_FAILS(status, "pjsip_tsx_send_msg failed", return status);

    return PJ_SUCCESS;
}

} // namespace sip_lib_internal 

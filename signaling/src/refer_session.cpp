#include "call/refer_session.h"

// http://tools.ietf.org/html/rfc5589
// http://www.in2eps.com/fo-sip/tk-fo-sip-service-04.html
// https://tools.ietf.org/html/rfc5359


namespace sip_lib_internal {

    refer_session::refer_session(shared_sip& a_shared, pjsip_dialog* a_dialog, pjsip_inv_session* a_session, sip_lib::call::ptr& a_call_face)
        : shared(a_shared)
        , dialog(a_dialog)
        , session(a_session)
        , call_face(a_call_face)
    {

    }


    refer_session::~refer_session()
    {

    }


    void refer_session::refer(std::string const& a_to)
    {
        if (!session)
            throw sip_lib::sip_exception("invite session not found");

        if (!dialog)
            throw sip_lib::sip_exception("pj dialog not found");

        pjsip_dlg_inc_lock(dialog);

        // Create xfer client subscription. 
        struct pjsip_evsub_user xfer_cb;
        pj_bzero(&xfer_cb, sizeof(xfer_cb));
        xfer_cb.on_evsub_state = &xfer_client_on_evsub_state;

        pjsip_evsub *sub = NULL;
        pj_status_t status = pjsip_xfer_create_uac(dialog, &xfer_cb, &sub);
        THROW_IF_STATUS_FAILS(status, "Unable to create transferer (sender of REFER request).");

        pjsip_evsub_set_mod_data(sub, 0, this);

        // Create REFER request.
        pjsip_tx_data *tdata = NULL;
        pj_str_t refer_to_uri = pj_str_cast(a_to);
        // Call this function to create request to initiate REFER subscription, to refresh subscription, 
        // or to unsubscribe. For request other than the initial REFER request, "refer_to_uri" argument may be NULL.
        status = pjsip_xfer_initiate(sub, &refer_to_uri, &tdata);
        THROW_IF_STATUS_FAILS(status, "create request to initiate REFER subscription");

        // Send
        status = pjsip_xfer_send_request(sub, tdata);
        THROW_IF_STATUS_FAILS(status, "Unable to send REFER");


        if (dialog)
            pjsip_dlg_dec_lock(dialog);
    }

    void refer_session::xfer_client_on_evsub_state(pjsip_evsub *sub, pjsip_event *event)
    {

        refer_session* _this = static_cast<refer_session*>(pjsip_evsub_get_mod_data(sub, 0));
        if (_this)
            _this->on_xfer_client_on_evsub_state(sub, event);

    }

    void refer_session::on_xfer_client_on_evsub_state(pjsip_evsub *sub, pjsip_event *event)
    {
        if (pjsip_evsub_get_state(sub) == PJSIP_EVSUB_STATE_ACCEPTED || pjsip_evsub_get_state(sub) == PJSIP_EVSUB_STATE_TERMINATED)
        {
            int code = event->body.tsx_state.tsx->status_code;
            call_face->on_transfer_result(code);
        }
    }


} // namespace sip_lib_internal 

// http://svn.pjsip.org/repos/pjproject/trunk/pjsip-apps/src/samples/simpleua.c

#include "call/siplib.h"
#include "call/logger.h"

char const* call_module_name = "Call Module";
char const* reg_module_name = "Register Module";
int Log::level = 1;


// netstat - aon | grep UDP

namespace sip_lib_internal {

    // workaround
    // pjsip library limitation, i cant to pass it into the on_rx_request or any other callback
    siplib* singleinstance = 0;

    void siplib::log( int level, const char *buf, int len )
    {
        std::string str(buf, len);
        Log::log(level, str.c_str());
    }

    class call_id_finder {
    public:
        call_id_finder(std::string const& a_call_id) 
            : call_id(a_call_id) {}
        bool operator() (invite_session::ptr const& a_call)
        { return call_id == a_call->call_face->call_id; }
        std::string call_id;
    };

    siplib::siplib(sip_lib::notifier & a_notify, sip_lib::account_t const& a_account)
        : pj_endpt(0)
        , to_stop(false)
        , notify(a_notify)
        , timer_heap(0)
        , account(a_account)
    {
        int status = pj_lib_init();
        if (status != PJ_SUCCESS)
        {
            LOGE( "Init PJLib: %d", status );
            throw sip_lib::sip_exception("PJLib init failure.");
        }

        pj_thread  = thread_ptr( new std::thread( &siplib::loop, this ) );
        
        registrar = register_session::ptr(new register_session(*this, a_account));
        im = im_session::ptr(new im_session(*this, a_account));
        singleinstance = this;
    }

    std::string const& siplib::network_interafce() const
    {
        return eth;
    }

    std::string siplib::create_contact(sip_lib::account_t const& a_account) const
    {
        std::string hostip = network_interafce();
        std::stringstream contact;

        contact << "<sip:" << a_account.user_name << "@" << hostip << ":";
        if (account.transport == sip_lib::account_t::tcp)
            contact << tcp_local_port << ";transport=tcp>";
        if (account.transport == sip_lib::account_t::udp)
            contact << udp_local_port << ";transport=udp>";

        return contact.str();
    }

    pj_status_t siplib::pj_lib_init()
    {
        pj_status_t status;

        pj_log_set_log_func( (pj_log_func*) &log );
        pj_log_set_decor( PJ_LOG_HAS_LEVEL_TEXT );
        pj_log_set_level( 5 );

        /* Must init PJLIB first: */
        status = pj_init();
        DO_IF_STATUS_FAILS(status, "pj_init failed ", return status);

        status = pjlib_util_init();
        DO_IF_STATUS_FAILS(status, "pjlib_util_init failed", return status);

        // init interface
        pj_caching_pool_init(&pj_cpool, &pj_pool_factory_default_policy, 0 );

        status = pjsip_endpt_create(&(pj_cpool.factory), "sip library", &pj_endpt);
        DO_IF_STATUS_FAILS(status, "Unable to create PJ endpoint", return status);

        pj_pool = pjsip_endpt_create_pool(pj_endpt, "pjsip_pool", 1000, 1000);
        DO_IF_STATUS_FAILS(status, "pjsip_endpt_create_pool failded", return status);

        transport_init();

        // common TSX, requres for new incoming messages
        status = pjsip_tsx_layer_init_module(pj_endpt);
        DO_IF_STATUS_FAILS(status, "Unable to start TSX layer module", return status);

        // requeres for both uac and uas
        status = pjsip_ua_init_module( pj_endpt, NULL );
        DO_IF_STATUS_FAILS(status, "Unable to pjsip_ua_init_module", return status);

        status = invite_init();
        DO_IF_STATUS_FAILS(status, "Unable to init invite", return status);

        status = register_init();
        DO_IF_STATUS_FAILS(status, "Unable to init register", return status);

        status = refer_init();
        DO_IF_STATUS_FAILS(status, "Unable to init refer", return status);

        status = im_init();
        DO_IF_STATUS_FAILS(status, "Unable to init im", return status);

        status = pj_timer_heap_create(pj_pool, 5, &timer_heap);
        DO_IF_STATUS_FAILS(status, "Unable to init timer pool", return status);

        return PJ_SUCCESS;
    }

    bool is_junk(char a)
    {
        return (a == ' ' /*|| a == '}'*/);
    }

    void siplib::transport_init()
    {

        pj_sockaddr local_addr;
        pj_status_t status = 0;

        if (account.transport == sip_lib::account_t::udp)
            status = pj_sockaddr_init(pj_AF_INET(), &local_addr, NULL, udp_local_port); // bind on any or udp_local_port local port
        if (account.transport == sip_lib::account_t::tcp)
            status = pj_sockaddr_init(pj_AF_INET(), &local_addr, NULL, tcp_local_port); // bind on any or tcp_local_port local port

        THROW_IF_STATUS_FAILS(status, "Unable to start transport");


        pjsip_transport* p_transport = 0;
        std::string local_host;
        unsigned short local_port = 0;

        status = -1;
        if (account.transport == sip_lib::account_t::udp)
        {
            status = pjsip_udp_transport_start(pj_endpt, &local_addr.ipv4, NULL, 1, &p_transport);
            THROW_IF_STATUS_FAILS(status, "Unable to start udp transport");
            local_host = std::string(p_transport->local_name.host.ptr, p_transport->local_name.host.slen);
            local_port = p_transport->local_name.port;
            udp_local_port = local_port;
        }
        if (account.transport == sip_lib::account_t::tcp)
        {

            pjsip_tpfactory* tpfactory = 0;
            pjsip_tp_state_listener_key* transport_listener_key = 0;

            status = pjsip_tcp_transport_start2(pj_endpt, &local_addr.ipv4, 0, 1, &tpfactory);
            THROW_IF_STATUS_FAILS(status, "Unable to start tcp transport");

            pjsip_tpmgr *mgr = pjsip_endpt_get_tpmgr(pj_endpt);
            pjsip_tpmgr_set_state_cb(mgr, &pjsip_tp_state_callback);

            local_host = std::string(tpfactory->addr_name.host.ptr, tpfactory->addr_name.host.slen);
            local_port = tpfactory->addr_name.port;
            tcp_local_port = local_port;
        }

        eth = local_host;

    }

    struct reg_failed : public sip_lib::async_operation_result
    {
        reg_failed(siplib* a_sp)
            : sp(a_sp)
        {}

        void error(std::exception const& ex)
        {
            LOGE("%s", ex.what());
            sp->registration_failed(PJSIP_SC_SERVICE_UNAVAILABLE);
        }
        virtual void success() {}
        siplib* sp; // dont worry about pointer, any async_operation_result can be called only from sip_lib::loop (one thread + one instance)
    };

    void siplib::pjsip_tp_state_callback(pjsip_transport *, pjsip_transport_state state, const pjsip_transport_state_info *info)
    {
        LOGD("TCP connection state %d ", state);

        if (state == PJSIP_TP_STATE_DISCONNECTED)
        {  
            if (singleinstance)
            {
                singleinstance->bind_register(reg_failed::ptr(new reg_failed(singleinstance)));
            }
        }

    }

    pj_status_t siplib::invite_init()
    {
        pj_status_t status;

        // initialize common Invite modules
        pj_bzero(&inv_session_event_handler, sizeof(inv_session_event_handler));
        inv_session_event_handler.on_state_changed = &invite_session::on_state_changed;
        inv_session_event_handler.on_new_session = &invite_session::on_new_session;
        inv_session_event_handler.on_media_update = &invite_session::on_media_update;
        status = pjsip_inv_usage_init(endpoint(), &inv_session_event_handler);
        DO_IF_STATUS_FAILS(status, "pjsip_inv_usage_init fails", return status);

        // http://www.ietf.org/rfc/rfc3262.txt
        status = pjsip_100rel_init_module(endpoint());
        DO_IF_STATUS_FAILS(status, "pjsip_100rel_init_module fails", return status);

        return PJ_SUCCESS;
    }

    pj_status_t siplib::register_init()
    {
        pj_status_t status;

        // initialize common Register modules
        pj_bzero( &register_event_handler, sizeof( register_event_handler) );
        register_event_handler.name            = pj_str_cast(reg_module_name);
        register_event_handler.id              = -1;
        register_event_handler.on_tsx_state    = register_session::on_tsx_state;
        register_event_handler.priority        = PJSIP_MOD_PRIORITY_APPLICATION;
        register_event_handler.on_rx_request   = &siplib::on_rx_request;

        status = pjsip_endpt_register_module( endpoint(), &register_event_handler);
        DO_IF_STATUS_FAILS(status, "Unable to create pjsip_endpt_register_module", return status);
        return PJ_SUCCESS;
    }

    pj_status_t siplib::im_init()
    {
        const pj_str_t msg_tag = pj_str_cast("MESSAGE");
        const pj_str_t STR_MIME_TEXT_PLAIN = pj_str_cast("text/plain");
        const pj_str_t STR_MIME_APP_ISCOMPOSING = pj_str_cast("application/im-iscomposing+xml");
        pj_status_t status;

        pj_bzero(&im_event_handler, sizeof(im_event_handler));
        im_event_handler.name = pj_str_cast("mod-im");
        im_event_handler.priority = PJSIP_MOD_PRIORITY_APPLICATION;
        im_event_handler.id = -1;

        status = pjsip_endpt_register_module(endpoint(), &im_event_handler);
        DO_IF_STATUS_FAILS(status, "Unable to create pjsip_endpt_register_module", return status);

        /* Register support for MESSAGE method. */
        pjsip_endpt_add_capability(endpoint(), &im_event_handler, PJSIP_H_ALLOW,
            NULL, 1, &msg_tag);

        /* Register support for "application/im-iscomposing+xml" content */
        pjsip_endpt_add_capability(endpoint(), &im_event_handler, PJSIP_H_ACCEPT,
            NULL, 1, &STR_MIME_APP_ISCOMPOSING);

        /* Register support for "text/plain" content */
        pjsip_endpt_add_capability(endpoint(), &im_event_handler, PJSIP_H_ACCEPT,
            NULL, 1, &STR_MIME_TEXT_PLAIN);

        return PJ_SUCCESS;
    }

    pj_status_t siplib::refer_init()
    {
        pj_status_t status;

        // This module provides the implementation of SIP Extension for SIP Specific 
        // Event Notification (RFC 3265). It extends PJSIP by supporting SUBSCRIBE and NOTIFY methods.
        status = pjsip_evsub_init_module(endpoint());
        DO_IF_STATUS_FAILS(status, "Unable to Initialize the Event Notification subsystem", return status);

        status = pjsip_xfer_init_module(endpoint());
        DO_IF_STATUS_FAILS(status, "Unable to Initialize the REFER subsystem", return status);

        // A User Agent that wishes to replace a single existing early or confirmed dialog with 
        // a new dialog of its own, MAY send the target User Agent an INVITE request containing a Replaces header field.
        // status = pjsip_replaces_init_module(endpoint());
        // DO_IF_STATUS_FAILS(status, "Unable to Initialize Replaces support in PJSIP.", return status);

        return PJ_SUCCESS;
    }


    void siplib::loop()
    {
        pj_time_val timeout = { 0, 300}; // // milliseconds
        pj_thread_t *thread;
        pj_thread_desc thread_desc;
        pj_bzero(thread_desc, sizeof(thread_desc));

        pj_status_t status = pj_thread_register( "siplib::loop", thread_desc, &thread );
        DO_IF_STATUS_FAILS(status, "pj_thread_register failed", return );

        while (!to_stop)
        {
            pjsip_endpt_handle_events( pj_endpt, &timeout );
            job fun;
            while(jobs.try_pop(fun))
            {
                try
                {
                    fun.fun();
                }
                // todo check that & avoid exception splitting
                catch (std::exception const& e)
                {
                    LOGE( e.what() );
                    if (fun.error)
                        fun.error->error(e);
                    continue;
                }
                if (fun.error)
                    fun.error->success();
            }

            try
            {
                pj_timer_heap_poll(timer_heap, NULL);
            }
            catch (std::exception & e)
            {
                // to do critical error, it's not good idea pass exception from callback through C code
                // may be call terminate? 
                LOGE(e.what());
            }

        }
    }


    siplib::~siplib(void)
    {
        singleinstance = 0;

        to_stop = true;

        pj_thread->join();

        registrar.reset();

        pj_timer_heap_destroy(timer_heap);
        timer_heap = 0;

        pjsip_endpt_unregister_module(endpoint(), &register_event_handler);
        pj_bzero(&register_event_handler, sizeof(register_event_handler));
        register_event_handler.id = -1;
        pj_bzero(&inv_session_event_handler, sizeof(inv_session_event_handler));

        pjsip_endpt_release_pool(pj_endpt, pj_pool);
        pj_pool = 0;

        pjsip_endpt_destroy(pj_endpt);
        pj_endpt = 0;
        pj_caching_pool_destroy(&pj_cpool);
        pj_shutdown();
    }

    void siplib::bind_register(sip_lib::async_operation_result::ptr result)
    {
        async_call(job(std::bind(&register_session::start, registrar.get()), result));
    }

    void siplib::unbind_register(sip_lib::async_operation_result::ptr result)
    {
        async_call(job(std::bind(&register_session::stop, registrar.get()), result));
    }

    void siplib::hang_up(sip_lib::call::ptr const& a_call, sip_lib::async_operation_result::ptr result)
    { 
        CALL_CHECK_THROW(a_call)
            async_call(job(std::bind(&siplib::on_hang_up, this, a_call), result));
    }

    void siplib::on_hang_up(sip_lib::call::ptr const& a_call)
    {
        calls_t::const_iterator ucall = find(a_call->call_id);
        if (ucall != calls.end())
            (*ucall)->hangup();
        else
            throw sip_lib::sip_exception("cant find call");
    }

    void siplib::answer(sip_lib::call::ptr const& a_call, sip_lib::async_operation_result::ptr result)
    { 
        CALL_CHECK_THROW(a_call)
            async_call(job(std::bind(&siplib::on_answer, this, a_call), result));
    }

    void siplib::on_answer(sip_lib::call::ptr const& a_call)
    {
        calls_t::const_iterator ucall = find(a_call->call_id);
        if (ucall != calls.end())
            (*ucall)->answer();
        else
            throw "cant find call";
    }

    void siplib::hold(sip_lib::call::ptr const& a_call, bool hold, sip_lib::async_operation_result::ptr result)
    {
        CALL_CHECK_THROW(a_call)
            async_call(job(std::bind(&siplib::on_hold, this, a_call, hold), result));
    }

    void siplib::on_hold(sip_lib::call::ptr const& a_call, bool hold)
    {
        calls_t::const_iterator ucall = find(a_call->call_id);
        if (ucall != calls.end())
            (*ucall)->hold(hold);
        else
            throw "cant find call";
    }

    void siplib::make_call(sip_lib::call::ptr const& a_call, std::string const& a_to, sip_lib::async_operation_result::ptr result)
    {
        CALL_CHECK_THROW(a_call)
            async_call(job(std::bind(&siplib::on_make_call, this, a_call, a_to), result));
    }

    void siplib::on_make_call(sip_lib::call::ptr const& a_call, std::string const& a_to)
    {
        invite_session::ptr user_call = invite_session::ptr(new invite_session(*this, a_to, a_call, account));
        push(user_call);
        user_call->invite();
    }

    void siplib::transfer(sip_lib::call::ptr const& a_call, std::string const& a_to, sip_lib::async_operation_result::ptr result)
    {
        CALL_CHECK_THROW(a_call)
            async_call(job(std::bind(&siplib::on_transfer, this, a_call, a_to), result));
    }

    void siplib::on_transfer(sip_lib::call::ptr const& a_call, std::string const& a_to)
    {
        calls_t::const_iterator ucall = find(a_call->call_id);
        if (ucall != calls.end())
            (*ucall)->transfer(a_to);
        else
            throw sip_lib::sip_exception("cant find call");
    }

    void siplib::send_voice_message(
        std::string const& a_to, 
        std::string const& text, 
        sip_lib::async_operation_result::ptr sending_result,
        sip_lib::sip_response::ptr answer)
    {
        async_call(job(std::bind(&siplib::on_send_voice_message, this, a_to, text, answer), sending_result));
    }

    void siplib::on_send_voice_message(std::string const& a_to, std::string const& text, sip_lib::sip_response::ptr answer)
    {
        im->send_voice_message(a_to, text, answer);
    }

    void siplib::free(std::string call_id)
    {
        calls_t::iterator ucall = find(call_id);
        if (ucall != calls.end())
            calls.erase(ucall);
    }

    pjsip_endpoint* siplib::endpoint() const
    { 
        return pj_endpt;
    }

    pj_pool_t* siplib::pool() const
    {
        return pj_pool;
    }

    void siplib::async_call(job const& fun) const
    {
        jobs.push(fun);
    }


    pj_bool_t siplib::on_rx_request(pjsip_rx_data *rdata)
    {
        pjsip_dialog* inc_dlg = pjsip_rdata_get_dlg( rdata );
        // filter only out of dialog invite requests
        if ( !inc_dlg && rdata->msg_info.msg->line.req.method.id == PJSIP_INVITE_METHOD )
            if(singleinstance)
                return singleinstance->rx_request(rdata);
        return PJ_FALSE;
    }

    pj_bool_t siplib::rx_request(pjsip_rx_data *rdata)
    {
        
        sip_lib::caller_info caller;
        pjsip_from_hdr* fhdr = (pjsip_from_hdr*)pjsip_msg_find_hdr(rdata->msg_info.msg, PJSIP_H_FROM, NULL);
        if (fhdr)
        {
            pjsip_sip_uri* fromUri = (pjsip_sip_uri*)pjsip_uri_get_uri(fhdr->uri);
            caller.user_name = std::string(fromUri->user.ptr, fromUri->user.slen);
        }
        sip_lib::call::ptr icall = notify.on_new_call(registrar->account, caller);

        invite_session::ptr incoming_call = invite_session::ptr(new invite_session(*this, rdata, registrar->account));
        pj_str_t cid = incoming_call->dialog->call_id->id;

        if (icall)
        {
            icall->call_id = std::string(cid.ptr, cid.slen);
            incoming_call->call_face = icall;

            push(incoming_call);
        }
        else
        {
            LOGE("Failed INVITE packet"); 
        }
        return PJ_TRUE;
    }

    void siplib::push(invite_session::ptr const& a_call)
    {
        calls.push_back(a_call);
    }

    siplib::calls_t::iterator siplib::find(std::string const& call_id)
    {
        return std::find_if(calls.begin(), calls.end(), call_id_finder(call_id));
    }

    pj_status_t siplib::schedule_timer(pj_timer_entry& timer, int time_out_sec) const
    {
        pj_time_val delay;
        delay.sec = time_out_sec;
        delay.msec = 0;
        return pj_timer_heap_schedule(timer_heap, &timer, &delay);
    }

    pj_status_t siplib::stop_timer(pj_timer_entry* timer, int id) const
    {
        return pj_timer_heap_cancel_if_active(timer_heap, timer, id);
    }

} // namespace sip_lib_internal 

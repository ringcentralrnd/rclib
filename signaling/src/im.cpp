
#include "call/siplib.h"


namespace sip_lib_internal {

    enum
    {
        PJSIP_MESSAGE_METHOD = PJSIP_OTHER_METHOD
    };

    const pjsip_method pjsip_message_method =
    {
        (pjsip_method_e)PJSIP_MESSAGE_METHOD,
        pj_str_cast("MESSAGE")
    };


    /* MIME constants. */
    static const pj_str_t STR_MIME_APP = pj_str_cast("application");
    static const pj_str_t STR_MIME_ISCOMPOSING = pj_str_cast("im-iscomposing+xml");
    static const pj_str_t STR_MIME_TEXT_PLAIN = pj_str_cast("text/plain");
    static const pj_str_t STR_AGENT = pj_str_cast("x-rc/agent");


    struct im_response
    {
        im_response(im_session* a_this, sip_lib::sip_response::ptr a_answer) 
            : _this(a_this)
            , answer(a_answer)
        {}
        im_session* _this;
        sip_lib::sip_response::ptr answer;
    };


    im_session::im_session(shared_sip const& a_shared, sip_lib::account_t const& a_account)
        : shared(a_shared)
    {
        account = a_account; // copy to avoid changes
        init_auth(auth_sess, shared, account);
    }


    void im_session::send_voice_message(std::string const& a_to, std::string const& text, sip_lib::sip_response::ptr answer)
    {
        pj_str_t pj_mime_text_plain;
        pj_strdup2_with_null(shared.pool(), &pj_mime_text_plain, STR_AGENT.ptr);
        std::string to_uri = account.uri(a_to, account.user_host);
        pj_str_t to_pj = pj_str_cast(to_uri);
        send(&to_pj, &pj_mime_text_plain, text.c_str(), answer);
    }


    void parse_media_type(pj_pool_t *pool, const pj_str_t *mime, pjsip_media_type *media_type)
    {
        pj_str_t tmp;
        char *pos;

        pj_bzero(media_type, sizeof(*media_type));

        pj_strdup_with_null(pool, &tmp, mime);

        pos = pj_strchr(&tmp, '/');
        if (pos) {
            media_type->type.ptr = tmp.ptr;
            media_type->type.slen = (pos - tmp.ptr);
            media_type->subtype.ptr = pos + 1;
            media_type->subtype.slen = tmp.ptr + tmp.slen - pos - 1;
        }
        else {
            media_type->type = tmp;
        }
    }


    void im_session::send(
        const pj_str_t *to,
        const pj_str_t *mime_type,
        const char* msg_data,
        sip_lib::sip_response::ptr answer)
    {
        pjsip_tx_data *tdata;
        pjsip_media_type media_type;
        pj_status_t status;
        std::string from_uri = account.uri();
        pj_str_t from = pj_str_cast(from_uri);

        status = pjsip_endpt_create_request(
            shared.endpoint(),
            &pjsip_message_method,
            to,
            &from,
            to, NULL, NULL, -1, NULL, &tdata);
        THROW_IF_STATUS_FAILS(status, "Unable to create request");


        pjsip_accept_hdr *accept;
        accept = pjsip_accept_hdr_create(shared.pool());
        accept->values[0] = STR_MIME_TEXT_PLAIN;
        accept->values[1] = STR_MIME_ISCOMPOSING;
        accept->count = 2;
        pjsip_msg_add_hdr(tdata->msg, (pjsip_hdr*)accept);

        /* Set default media type if none is specified */
        if (mime_type == NULL)
            mime_type = &STR_MIME_TEXT_PLAIN;

        parse_media_type(tdata->pool, mime_type, &media_type);

        /* Add message body */
        pj_str_t body_text = pj_str_cast(msg_data);
        tdata->msg->body = pjsip_msg_body_create(tdata->pool, &media_type.type,
            &media_type.subtype,
            &body_text);
        if (tdata->msg->body == NULL)
        {
            pjsip_tx_data_dec_ref(tdata);
            THROW_IF_STATUS_FAILS(PJ_ENOMEM, "Unable to create request");
        }

        pjsip_route_hdr *route = route_hdr(shared, account);
        pjsip_msg_add_hdr(tdata->msg, (pjsip_hdr*)route);

        im_response* token = new im_response(this, answer);
        status = pjsip_endpt_send_request(shared.endpoint(), tdata, -1, token, &im_session::im_callback);
        DO_IF_STATUS_FAILS(status, "send im failed", return)
        THROW_IF_STATUS_FAILS(status, "Unable to send im request");
    }

    void im_session::im_callback(void *user_data, pjsip_event *event)
    {
        im_response* _this = 0;
        try
        {
            if (!user_data)
                return;
            _this = static_cast<im_response*>(user_data);
            _this->_this->on_im_callback(event, _this->answer);
        }
        catch (std::exception & e)
        {
            LOGE(e.what());
        }
        delete _this;
    }


    void im_session::on_im_callback(pjsip_event *e, sip_lib::sip_response::ptr answer)
    {

        if (e->type == PJSIP_EVENT_TSX_STATE) {

            pjsip_transaction *tsx = e->body.tsx_state.tsx;


            answer->on_result(tsx->status_code);

            /* Ignore provisional response, if any */
            if (tsx->status_code < 200)
                return;


            /*
            if (e->body.tsx_state.type == PJSIP_EVENT_RX_MSG &&
            (tsx->status_code == 401 || tsx->status_code == 407))
            {
            pjsip_rx_data *rdata = e->body.tsx_state.src.rdata;
            pjsip_tx_data *tdata;
            pjsip_auth_clt_sess auth;
            pj_status_t status;

            //PJ_LOG(4,(THIS_FILE, "Resending IM with authentication"));

            pjsip_auth_clt_init(&auth, shared.endpoint(), rdata->tp_info.pool, 0);

            pjsip_auth_clt_set_credentials(&auth,
            pjsua_var.acc[im_data->acc_id].cred_cnt,
            pjsua_var.acc[im_data->acc_id].cred);

            pjsip_auth_clt_set_prefs(&auth,
            &pjsua_var.acc[im_data->acc_id].cfg.auth_pref);

            status = pjsip_auth_clt_reinit_req(&auth, rdata, tsx->last_tx,
            &tdata);
            if (status == PJ_SUCCESS) {
            pjsua_im_data *im_data2;

            im_data2 = pjsua_im_data_dup(tdata->pool, im_data);

            PJSIP_MSG_CSEQ_HDR(tdata->msg)->cseq++;

            status = pjsip_endpt_send_request(pjsua_var.endpt, tdata, -1,
            im_data2, &im_callback);
            if (status == PJ_SUCCESS) {
            return;
            }
            }
            }
            */
            if (tsx->status_code / 100 == 2) {
                //	    PJ_LOG(4,(THIS_FILE, "Message \'%s\' delivered successfully", im_data->body.ptr));
            }
            else {
                //	    PJ_LOG(3,(THIS_FILE, "Failed to deliver message \'%s\': %d/%.*s",
                //		      im_data->body.ptr,
                //		      tsx->status_code,
                //		      (int)tsx->status_text.slen,
                //		      tsx->status_text.ptr));
            }
        }
    }


    im_session::~im_session()
    {
    }


} // namespace sip_lib_internal 

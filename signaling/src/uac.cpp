#include "call/uac.h"
#include "call/siplib.h"

namespace sip_lib {

    uac::uac() 
    {
    }

    uac::~uac() 
    {
        sip_impl.reset();
    }

    void uac::init(notifier& a_notify, account_t const& a_account)
    {
        if(sip_impl.get())
            throw sip_exception("cant init lib twice");
        sip_impl = sip_lib_internal::siplib_prt(new sip_lib_internal::siplib(a_notify, a_account));
    }
    
    std::string uac::network_interafce() const
    {
        if(!sip_impl)
            throw sip_exception("not initialized");
        return sip_impl->network_interafce();
    }

    // void uac::change_accout(account_t const& a_account)
    // {
    //     if(!sip_impl)
    //         throw sip_exception("not initialized");
    //     sip_impl->change_accout(a_account);
    // }

    void uac::bind_register(sip_lib::async_operation_result::ptr result)
    {
        if(!sip_impl)
            throw sip_exception("not initialized");
        sip_impl->bind_register(result);
    }

    void uac::unbind_register(sip_lib::async_operation_result::ptr result)
    {
        if(!sip_impl)
            throw sip_exception("not initialized");
        sip_impl->unbind_register(result);
    }

    void uac::make_call(call::ptr const& a_call, std::string const& to, sip_lib::async_operation_result::ptr result)
    {
        if(!sip_impl)
            throw sip_exception("not initialized");
        sip_impl->make_call(a_call, to, result);
    }

    void uac::hang_up(call::ptr const& a_call, sip_lib::async_operation_result::ptr result)
    {
        if(!sip_impl)
            throw sip_exception("not initialized");
        sip_impl->hang_up(a_call, result);
    }

    void uac::answer(call::ptr const& a_call, sip_lib::async_operation_result::ptr result)
    {
        if(!sip_impl)
            throw sip_exception("not initialized");
        sip_impl->answer(a_call, result);
    }

    void uac::hold(call::ptr const& a_call, bool hold, sip_lib::async_operation_result::ptr result)
    {
        if (!sip_impl)
            throw sip_exception("not initialized");
        sip_impl->hold(a_call, hold, result);
    }

    void uac::transfer(call::ptr const& a_call, std::string const& to, sip_lib::async_operation_result::ptr result)
    {
        if (!sip_impl)
            throw sip_exception("not initialized");
        sip_impl->transfer(a_call, to, result);
    }

    void uac::send_voice_message(
            std::string const& a_to,
            std::string const& text,
            sip_lib::async_operation_result::ptr sending_result,
            sip_lib::sip_response::ptr answer)
    {
        if (!sip_impl)
            throw sip_exception("not initialized");
        sip_impl->send_voice_message(a_to, text, sending_result, answer);
    }



} // namespace sip_lib

#include "call/invite_session.h"
#include "call/siplib.h"
#include "call/sdp.h"

namespace sip_lib_internal {

    // incoming invite
    invite_session::invite_session(shared_sip& a_shared, pjsip_rx_data *request_data, sip_lib::account_t const& a_account)
        : shared(a_shared)
        , session(0) 
        , dialog(0)
    {
        account = a_account; // copy to avoid changes
        contact = shared.create_contact(account);
        pj_str_t contact_uri = pj_str_cast(contact);
        pjsip_tx_data* tdata = 0;
        pj_status_t status;
        unsigned options = 0;

        // Application SHOULD call this function upon receiving the initial INVITE request 
        // in rdata before creating the invite session (or even dialog), to verify that the invite session can handle the INVITE request.
        status = pjsip_inv_verify_request( request_data, &options, NULL, NULL, shared.endpoint(), &tdata );
        THROW_IF_STATUS_FAILS(status,  "Invalid incoming request");

        status = pjsip_dlg_create_uas( pjsip_ua_instance(), request_data, &contact_uri, &dialog );
        THROW_IF_STATUS_FAILS(status,  "Cant create the dialog for outgoing INVITE");

        // Create UAS invite session 
        status = pjsip_inv_create_uas(dialog, request_data, NULL, 0, &session);
        if (status != PJ_SUCCESS) 
        {
            pjsip_dlg_create_response(dialog, request_data, PJSIP_SC_INTERNAL_SERVER_ERROR, NULL, &tdata);
            pjsip_dlg_send_response(dialog, pjsip_rdata_get_tsx(request_data), tdata);
            return;
        }

        //      saveIncInvite( invSessionInc, requestInvite.tsxKey().get(), CSSObjectEventDataPtr( new CSSObjectEventData( requestInvite ) ) );

        session->mod_data[0] = this;
        // dont need it should be automaticly?
        // Send 100/Trying 
        status = pjsip_inv_initial_answer( session, request_data, 100, NULL, NULL, &tdata);
        THROW_IF_STATUS_FAILS(status,  "Cant create 100 trying");
        status = pjsip_inv_send_msg( session, tdata); 
        if ( status != PJ_SUCCESS ) 
        {
            pjsip_tx_data_dec_ref( tdata );
            THROW_IF_STATUS_FAILS(status,  "Cant send 100 trying");
        }

       refer = refer_session::ptr(new refer_session(shared, dialog, session, call_face));

       if(!incomong_call_media_conversation())
       { // send error for unknown media
           pjsip_dlg_create_response(dialog, request_data, PJSIP_SC_NOT_ACCEPTABLE, NULL, &tdata);
           pjsip_dlg_send_response(dialog, pjsip_rdata_get_tsx(request_data), tdata);
           return;
       }
    }

    bool invite_session::incomong_call_media_conversation()
    {
        sip_lib::media_offer_info offer = extract_offer(session);
        media.connection = offer.connection;
        media.codec = shared.codec_selection_answer(offer.codecs);
        return !media.codec.payload.empty();
    }

    void invite_session::answer()
    {
        send_response();
    }

    pj_status_t invite_session::send_response()
    {
        pjsip_tx_data *tdata(0);
        pj_status_t status;

        pjmedia_sdp_session* sdp;
        sip_lib::codec_list codecs;
        codecs.push_back(media.codec);
        LOGI("Answer codec is: %s", media.codec.mime_name.c_str());
        sip_lib_internal::make_answer(shared, call_face, &sdp, codecs);

        status = pjsip_inv_answer( session, 200, NULL, sdp, &tdata );
        if ( status != PJ_SUCCESS ) 
        {
            if (tdata)
                pjsip_tx_data_dec_ref( tdata );
            return status;
        }

        status = pjsip_inv_send_msg( session, tdata ); 
        if ( status != PJ_SUCCESS ) 
        {
            if (tdata)
                pjsip_tx_data_dec_ref( tdata );
            return status;
        }

        return status;
    }

    // try outgoing invite
    invite_session::invite_session(shared_sip& a_shared, std::string const& a_to, sip_lib::call::ptr const& a_call, sip_lib::account_t const& a_account)
        : shared(a_shared)
        , session(0) 
        , dialog(0)
        , call_face(a_call)
    {

        account = a_account;
        contact = shared.create_contact(account);

        std::string from = account.uri();
        pj_str_t from_addr = pj_str_cast(from);
        std::string to_uri = account.uri(a_to, account.user_host);
        pj_str_t to_addr = pj_str_cast(to_uri);
        pj_str_t contact_uri = pj_str_cast(contact);

        std::string req_uri_str = account.uri(a_to,account.user_host);
        pj_str_t req_uri = pj_str_cast(req_uri_str);

        pj_status_t status;

        status = pjsip_dlg_create_uac(pjsip_ua_instance(),
            &from_addr,	            // (From header)
            &contact_uri,	    //local Contact
            &to_addr,		            //remote URI (To header)
            &req_uri,		            //remote target
            &dialog);	            //dialog

        THROW_IF_STATUS_FAILS(status,  "Unable to create UAC dialog");
        init_auth(dialog->auth_sess, shared, account);

        status = pjsip_inv_create_uac(dialog, 0, 0, &session);
        THROW_IF_STATUS_FAILS(status,  "Unable to create UAC");
        session->mod_data[0] = this;

        std::stringstream ss2;
        if (account.transport == sip_lib::account_t::tcp)
            ss2 << "sip:" << account.proxy << ";transport=tcp" << ";lr";
        if (account.transport == sip_lib::account_t::udp)
            ss2 << "sip:" << account.proxy << ";transport=udp" << ";lr";
        std::string proxy_str = ss2.str();
        pjsip_route_hdr *route;
        const pj_str_t hname = pj_str_cast("Route");
        pj_str_t tmp;
        pj_strdup2_with_null(shared.pool(), &tmp, proxy_str.c_str());
        pj_list_init(&route_set);
        route = static_cast<pjsip_route_hdr *> (pjsip_parse_hdr(dialog->pool, &hname, tmp.ptr, tmp.slen, NULL));
        pj_list_push_back(&route_set, route);
        pjsip_dlg_set_route_set(dialog, &route_set);

        pj_str_t cid = dialog->call_id->id;
        call_face->call_id = std::string(cid.ptr, cid.slen);

        refer = refer_session::ptr(new refer_session(shared, dialog, session, call_face));
    }

    invite_session::~invite_session()
    {
        LOGI("free session");
    }

    void invite_session::compose_and_send_invite()
    {
        pjsip_tx_data* tdata = 0;
        pj_status_t status = 0;

        status = pjsip_inv_invite(session, &tdata);
        THROW_IF_STATUS_FAILS(status, "Unable to create invite TX");

        // add agent
        const pj_str_t USER_AGENT = pj_str_cast("User-Agent");
        const pj_str_t USER_AGENT_NAME = pj_str_cast("RC-Mobile-Agent");
        pjsip_hdr* h = (pjsip_hdr*)pjsip_user_agent_hdr_create(shared.pool(), &USER_AGENT, &USER_AGENT_NAME);
        pjsip_msg_add_hdr(tdata->msg, h);

        // check ToTag FromTag CallId for reinvite

        // set CSeq

        status = pjsip_inv_send_msg(session, tdata);
        THROW_IF_STATUS_FAILS(status, "Unable to send invite");
    }

    void invite_session::invite()
    {
        pj_status_t status = 0;

        // setup sdp
        pjmedia_sdp_session* sdp;
        initial_offer(shared, call_face, &sdp, shared.codec_selection_offer());
        status = pjsip_inv_set_local_sdp(session, sdp);
        THROW_IF_STATUS_FAILS(status, "Unable to set SDP");

        compose_and_send_invite();
    }


    void invite_session::hold(bool hold)
    {
        pj_status_t status = 0;

        // setup sdp
        pjmedia_sdp_session* sdp;
        hold_offer(shared, call_face, &sdp, hold);
        status = pjsip_inv_set_local_sdp(session, sdp);
        THROW_IF_STATUS_FAILS(status, "Unable to set SDP");
        compose_and_send_invite();
    }


    void invite_session::on_new_session(pjsip_inv_session *inv, pjsip_event *e)
    {
        PJ_UNUSED_ARG(e);
        PJ_UNUSED_ARG(inv);
    }

    void invite_session::on_state_changed( pjsip_inv_session *inv, pjsip_event *e)
    {
        invite_session* _this = static_cast<invite_session*>(inv->mod_data[0]);
        if(_this)
            _this->state_changed(inv, e);
    }

    void invite_session::state_changed( pjsip_inv_session *inv, pjsip_event *e)
    {
        PJ_UNUSED_ARG(e);

        if(call_face)
        {
            if (inv->state == PJSIP_INV_STATE_DISCONNECTED) 
            {
                pj_str_t cid = inv->dlg->call_id->id;
                sip_lib::reason res;
                res.code = inv->cause;
                res.source = sip_lib::reason::ESIP;
                call_face->on_terminated(std::string(cid.ptr, cid.slen), res);
                shared.free(std::string(cid.ptr, cid.slen));
            } 

            if (inv->state == PJSIP_INV_STATE_CONNECTING) 
            {        
                pj_str_t cid = inv->dlg->call_id->id;
                call_face->on_accepted(std::string(cid.ptr, cid.slen));
            }

            if (inv->state == PJSIP_INV_STATE_INCOMING) 
            {        
                pj_str_t cid = inv->dlg->call_id->id;
                call_face->on_accepted(std::string(cid.ptr, cid.slen));
            }

            if (inv->state == PJSIP_INV_STATE_CALLING)
            {
            //    assert(inv->dlg->state == PJSIP_DIALOG_STATE_ESTABLISHED);
            }
        }
    }

    void invite_session::on_media_update( pjsip_inv_session *inv, pj_status_t status)
    {
        try
        {
            if( !inv )
            {
                LOGE( "inv pointer is NULL" );
                return;
            }

            invite_session* _this = static_cast<invite_session*>(inv->mod_data[0]);
            if(_this)
                _this->media_update(inv,status);

        }
        catch (std::exception & e)
        {
            LOGE( e.what() );
        }
    }

    void invite_session::media_update( pjsip_inv_session *inv, pj_status_t status)
    {
        LOGD( "media updated" );

        DO_IF_STATUS_FAILS(status,  "SDP negotiation has failed", return);

        bool is_answer = inv->invite_tsx->role == PJSIP_ROLE_UAC;

        if (!is_answer)
        { 
            // need nothing , media is filled 
            if (call_face)
                call_face->on_media(media);
        }
        else
        {   // 200 or 183 answer
            // extract offer
            // chouse one (first) codec
            media = extract_answer(inv);
            if (call_face)
                call_face->on_media(media);
        }

    }


    void invite_session::hangup()
    {
        pj_status_t status = hangup_session();
        THROW_IF_STATUS_FAILS(status,  "Unable to stop invite");
    }

    pj_status_t invite_session::hangup_session()
        // PJ_DEF(pj_status_t) pjsip_inv_end_session(  pjsip_inv_session *inv,
        // 					    int st_code,
        // 					    const pj_str_t *st_text,
        // 					    pjsip_tx_data **p_tdata )
    {
        pjsip_tx_data *tdata(0);

        if( !session )
            throw sip_lib::sip_exception("Invite session is not initialized");

        if (!dialog)
            throw sip_lib::sip_exception( "pj dialog is not initialized" ); 

        //     pjsip_tx_data *tdata;
        //     THROW_IF_STATUS_FAILS(pjsip_dlg_create_request( dlg, &pjsip_bye_method, -1, &tdata ), "Cant create BYE request");
        //     THROW_IF_STATUS_FAILS(pjsip_inv_send_msg(session, tdata),  "Unable to send bye");

        pj_status_t status;

        switch (session->state) 
        {
        case PJSIP_INV_STATE_CALLING:
        case PJSIP_INV_STATE_EARLY:
        case PJSIP_INV_STATE_INCOMING:

            if (session->role == PJSIP_ROLE_UAC) 
            {
                /* For UAC when session has not been confirmed, create CANCEL. */
                /* MUST have the original UAC INVITE transaction. */
                PJ_ASSERT_RETURN(session->invite_tsx != NULL, PJ_EBUG);
                /* But CANCEL should only be called when we have received a
                * provisional response. If we haven't received any responses,
                * just destroy the transaction.
                */
                if (session->invite_tsx->status_code < 100) 
                {
                    /* Do not stop INVITE retransmission, see ticket #506 */
                    //pjsip_tsx_stop_retransmit(inv->invite_tsx);
                    session->cancelling = PJ_TRUE;
                    session->pending_cancel = PJ_TRUE;
                    PJ_LOG(4, (session->obj_name, "Delaying CANCEL since no "
                        "provisional response is received yet"));
                    return PJ_SUCCESS;
                }

                /* The CSeq here assumes that the dialog is started with an
                * INVITE session. This may not be correct; dialog can be 
                * started as SUBSCRIBE session.
                * So fix this!
                */
                status = pjsip_endpt_create_cancel(session->dlg->endpt, 
                    session->invite_tsx->last_tx,
                    &tdata);
                if (status != PJ_SUCCESS)
                    return status;

            } else {

                /* For UAS, send a final response. */
                tdata = session->invite_tsx->last_tx;
                PJ_ASSERT_RETURN(tdata != NULL, PJ_EINVALIDOP);

                status = pjsip_inv_answer(session, 503, NULL, NULL, &tdata);
            }
            break;

        case PJSIP_INV_STATE_CONNECTING:
        case PJSIP_INV_STATE_CONFIRMED:

            /* For established dialog, send BYE */
            status = pjsip_dlg_create_request(session->dlg, pjsip_get_bye_method(), 
                -1, &tdata);
            break;

        case PJSIP_INV_STATE_DISCONNECTED:
            /* No need to do anything. */
            return PJSIP_ESESSIONTERMINATED;

        default:
            pj_assert("!Invalid operation!");
            return PJ_EINVALIDOP;
        }


        if (status == PJ_SUCCESS)
            status = pjsip_inv_send_msg(session, tdata);

        return status;
    }


    void invite_session::transfer(std::string const& a_to)
    {
        std::string refer_uri = sip_lib::account_t::uri(a_to, account.user_host);
        refer->refer(refer_uri);
    }

} // namespace sip_lib_internal

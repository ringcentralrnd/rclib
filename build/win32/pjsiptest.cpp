#include <string>
#include <stdarg.h>  
#include <time.h>
#include <mutex>
#include <chrono>
#include <thread>
#include <condition_variable>

#include "call/uac.h"
#include "call/call.h"
#include "call/logger.h"

// todo include must be lib/name.h
// netstat -aon | grep TCP

sip_lib::media_info med = {"192.168.19.75", 12345};
std::condition_variable invite_waiter;
std::condition_variable end_waiter;
std::condition_variable register_waiter;
std::condition_variable answer_waiter;
std::condition_variable message_waiter;
std::mutex mtx;

int term_reason = 0;

class test_invite : public sip_lib::call
{
public:

    void on_accepted(std::string const& call_id) const
    {
        LOGI("on_accepted");
        answer_waiter.notify_one();
    }

    void on_terminated(std::string const& call_id, sip_lib::reason const& rs) const
    {
        LOGI("on_terminated");
        term_reason = rs.code;
        end_waiter.notify_one();
    }

    void on_media(sip_lib::media_info const& /*a_media*/) const
    {
        LOGI("on_media");
    }

    virtual sip_lib::media_info const& local_media() const
    {
        return med;
    }

    virtual void on_transfer_result(int err_sip_code) const
    {}


    ~test_invite()
    {
        term_reason = 0;
        LOGI("free call");
    }
};

test_invite::ptr test_call_0; 

int register_code = 0;

class mnotifier : public sip_lib::notifier
{
public:
    sip_lib::call::ptr on_new_call(sip_lib::account_t const& /*a_account*/, sip_lib::caller_info const& /*caller*/)
    {
        LOGD("************* INCOMING CALLL **************");
        {
            test_call_0 = test_invite::ptr(new test_invite);
        }
        invite_waiter.notify_one();
        return test_call_0;
    }
    virtual void on_registered(int exp_time_sec) 
    {
        register_code = 200;
        register_waiter.notify_one();
    };
    virtual void on_register_fails(int err_sip_code) 
    {
        register_code = err_sip_code;
        register_waiter.notify_one();
    };

    sip_lib::codec_description empty;

    virtual sip_lib::codec_description const& codec_selection_answer(sip_lib::codec_list const& offer) const
    {
        if (offer.empty())
            return empty;
        else
            return offer.front();
    }

    virtual sip_lib::codec_list codec_selection_offer() const
    {
        sip_lib::codec_list initial;
        initial.push_back(sip_lib::PCMU_CODEC);
        initial.push_back(sip_lib::HOME_MADE_OPUS_CODEC);
        return initial;
    }

} nt;

class res : public sip_lib::async_operation_result
{
    void error(std::exception const& ex)
    {
        LOGD("�����: %s", ex.what());
        try
        {
            throw;
        }
        catch (sip_lib::sip_exception const& e)
        {
            LOGE("��� ���� ����� %s", e.what());
        }
        catch (std::exception const& e)
        {
            LOGE("������ ������ %s", e.what());
        }
    }
    virtual void success()
    {
        LOGD("�������");
    }
};

sip_lib::uac::ptr a;


void wait_for_incoming_call()
{
    std::unique_lock<std::mutex> lock(mtx);
    invite_waiter.wait(lock);
}

void wait_for_bye()
{
    std::unique_lock<std::mutex> lock(mtx);
    end_waiter.wait(lock);
}


void wait_for_register()
{
    std::unique_lock<std::mutex> lock(mtx);
    register_waiter.wait(lock);
}

void wait_for_answer()
{
    std::unique_lock<std::mutex> lock(mtx);
    answer_waiter.wait(lock);
}

void wait_for_message()
{
    std::unique_lock<std::mutex> lock(mtx);
    message_waiter.wait(lock);
}


void transfer_test();
void hold_test();
void outgoing_call_test();
void incoming_call_test();


struct async_operation_result_ex : public sip_lib::async_operation_result
{
    void error(std::exception const& ex) {};
    void success() {};
};

struct sip_response_ex : public sip_lib::sip_response
{
    void on_result(unsigned short code)
    {
        message_waiter.notify_one();
    }
};

int main(int argc, char* argv[])
{
      sip_lib::account_t acc1;
      acc1.user_name = "12052160037";
      acc1.user_alias = "OLEG PgTest";
      acc1.user_host = "sip-rndmsg.lab.nordigy.ru";
      acc1.auth_user = "400017433008";
      acc1.password = "400017433008";
      acc1.proxy = "sip-rndmsg.lab.nordigy.ru:5090"; 
      acc1.transport = sip_lib::account_t::udp;

    a = sip_lib::uac::ptr(new sip_lib::uac());
    a->init(nt, acc1);

    // reinit test
    // a->init(nt, acc1);
    // a.reset();

    // register
    register_code = 0;
    res::ptr r(new res());
    a->unbind_register(r);
    wait_for_register();
    if (register_code != 200)
        return 1;

    register_code = 0;
    a->bind_register(r);
    wait_for_register();
    if (register_code != 200)
        return 1;

    async_operation_result_ex::ptr res(new async_operation_result_ex());
    sip_response_ex::ptr answ(new sip_response_ex());
    a->send_voice_message("12345", "test", res, answ);
    wait_for_message();
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));

//    hold_test();
//    incoming_call_test();
//  transfer_test();
    outgoing_call_test();

    // test reconnect
    // wait failed register
/*    LOGD("wait failed");
    wait_for_register();
    boost::this_thread::sleep(boost::posix_time::milliseconds(10000));
    LOGD("start new reg");
    a->bind_register(r);
    boost::this_thread::sleep(boost::posix_time::milliseconds(200000));
*/
    // Hold tests
    //    boost::this_thread::sleep(boost::posix_time::milliseconds(25000));
    // if (test_call_0)
    // a->answer(test_call_0);
    // a->hold(test_call, true);
    // boost::this_thread::sleep(boost::posix_time::milliseconds(2000));
    // a->hold(test_call, false);
    a.reset();
    return 0;
}

void hold_test()
{
    //  outgoing call test
    test_invite::ptr test_call_1;
    test_call_0 = test_invite::ptr(new test_invite);
    a->make_call(test_call_0, "12139720028");
    wait_for_answer();
    std::this_thread::sleep_for(std::chrono::milliseconds(20000));
    a->hold(test_call_0, true);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    a->hold(test_call_0, false);
    std::this_thread::sleep_for(std::chrono::milliseconds(20000));
    if (test_call_0)
        a->hang_up(test_call_0);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
}

void outgoing_call_test()
{
    //  outgoing call test
    test_invite::ptr test_call_1;
    test_call_0 = test_invite::ptr(new test_invite);
//    a->make_call(test_call_0, "14805130011");
    a->make_call(test_call_0, "12139720028");
    wait_for_answer();
    std::this_thread::sleep_for(std::chrono::milliseconds(20000));

    if(test_call_0)
        a->hang_up(test_call_0);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
}

void incoming_call_test()
{
    wait_for_incoming_call();
    if (test_call_0)
        a->answer(test_call_0);
    std::this_thread::sleep_for(std::chrono::milliseconds(10000));
    a->hang_up(test_call_0);
}

void transfer_test()
{
    // Transferee: the party being transferred to the Transfer Target.
    // Transferor:        the party initiating the transfer.
    // Transfer Target:   the new party being introduced into a call with the Transferee.
    char const* transfer_target = "12052670035";


    test_call_0.reset();

    // attended transfer
    wait_for_incoming_call();
    a->answer(test_call_0);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    a->transfer(test_call_0, transfer_target);
    wait_for_bye();
    assert(term_reason == 200);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    test_call_0.reset();

    // error transfer
    // start before dialog
    /*
    wait_for_incoming_call();
    boost::this_thread::sleep(boost::posix_time::milliseconds(2000));
    a->transfer(test_call_0, transfer_target);
    wait_for_bye();
    assert(term_reason == 487);
    test_call_0.reset();
    */
}
